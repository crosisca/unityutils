﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Carrega o nível de forma assíncrona, colocando um tempo mínimo para o carregamento.
/// É bom para criar telas de carregamento com logo de empresas.
/// </summary>
public class OpeningScene : MonoBehaviour{
	public string levelName;
	public float minimumTimeForWait = 2.5f;

	private AsyncOperation asyncOperation;
	private float beforeLoadTime;

	void Start(){
		StartCoroutine(Load());
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}
		
	IEnumerator Load() {
		float beforeLoadTime = Time.realtimeSinceStartup;
        AsyncOperation asyncOperation = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(levelName);
		asyncOperation.allowSceneActivation = false;
		while(asyncOperation.progress<0.9f || Time.realtimeSinceStartup-beforeLoadTime<minimumTimeForWait){
			yield return new WaitForSeconds(0.1f);
		}
		asyncOperation.allowSceneActivation = true;
	}
}
