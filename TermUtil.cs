﻿using UnityEngine;
using System.Collections;

public class TermUtil {

    const string MANHA = "Manhã";
    const string TARDE = "Tarde";
    const string INTEGRAL = "Integral";

    public static string CharToString(char c)
    {
        string term = "";
        switch (c)
        {
            case 'M' :
                term = MANHA;
            break;
            case 'T':
                term = TARDE;
            break;
            case 'I':
                term = INTEGRAL;
            break;
        }
        return term;
    }

    public static char StringToChar(string s)
    {
        char c = '\0';
        switch (s)
        {
            case MANHA:
                c = 'M';
                break;
            case TARDE:
                c = 'T';
                break;
            case INTEGRAL:
                c = 'I';
                break;
        }
        return c;
    }
	
}
