# unityutils
Repositório para guardar todos os packages que podem ser reutilizados em diversos projetos.

O repositório é equivalente à pasta "Assets" de um projeto Unity. Mantenham o projeto inteiro compilando, para tanto adicione as dependências externas (como a NGUI) numa pasta com o nome "3rdParty" que está listada no .gitignore.
