﻿using System.Threading;
using System;
using System.Net;
using UnityEngine;
using System.Collections;

namespace ConnectionTools {
	public enum ConnectionStatus {
		NotRunning,
		Connecting,
		Connected,
		NoConnection,
		WebServersDown
	}
	
	#region Http Connection Test
	public static class HttpConnectionTest {
		static Thread connectionThread;
		
		static int connectedRate = 40000;
		static int disconnectedRate = 5000;
		
		static string currentWebsite;
		public static ConnectionStatus CurrentStatus = ConnectionStatus.NotRunning;
		public static string errorMessage;
		
		
		/// <summary>
		/// Starts the connection check thread.
		/// </summary>
		/// <param name="website">Sets a website to perform a connection test, which will determine the server status.</param>
		public static void StartConnectionCheck (string website = default(string)) {
			if (connectionThread == null) {
				connectionThread = new Thread (new ThreadStart(ConnectionCheck));
			}
			
			currentWebsite = website;
			
			if (connectionThread.ThreadState == ThreadState.Aborted || connectionThread.ThreadState == ThreadState.Unstarted) {
				connectionThread.Start();
			}
		}
		
		
		/// <summary>
		/// Stops the connection check thread.
		/// </summary>
		public static void StopConnectionCheck () {
			if (connectionThread != null) {
				CurrentStatus = ConnectionStatus.NotRunning;
				connectionThread.Abort ();
			} 
		}
		
		
		static void ConnectionCheck()
		{
			CurrentStatus = ConnectionStatus.Connecting;

			HttpWebRequest gRequest;
			HttpWebRequest aRequest;
			while (true) {
				try {
					gRequest = (HttpWebRequest)WebRequest.Create ("http://www.google.com");
					aRequest = (HttpWebRequest)WebRequest.Create (currentWebsite);
				} catch (UriFormatException e) {
					errorMessage = e.Message + " -> " + e.StackTrace;
					Debug.LogError ("URL malformed:\n" + errorMessage);
					StopConnectionCheck ();
					return;
				}

				try {
					HttpWebResponse gResponse = (HttpWebResponse)gRequest.GetResponse();
					
					if (gResponse.StatusCode == HttpStatusCode.OK)
					{
						if (!string.IsNullOrEmpty(currentWebsite)) {
							try
							{
								HttpWebResponse aResponse = (HttpWebResponse)aRequest.GetResponse();
								
								if (aResponse.StatusCode == HttpStatusCode.OK)
								{
									CurrentStatus = ConnectionStatus.Connected;
									
								}
								else
								{                    
									CurrentStatus = ConnectionStatus.WebServersDown;
									
								}
							}
							catch (Exception e)
							{             
								aRequest.Abort ();
								CurrentStatus = ConnectionStatus.WebServersDown;
								errorMessage = e.Message + " -> " + e.StackTrace;
							}
							
						} else {
							CurrentStatus = ConnectionStatus.Connected;
						}
						
					} else {
						CurrentStatus = ConnectionStatus.NoConnection;
					}
					
				} catch (Exception e) {
					gRequest.Abort ();
					CurrentStatus = ConnectionStatus.NoConnection;
					errorMessage = e.Message + " -> " + e.StackTrace;
				}
				
				Thread.Sleep (CurrentStatus != ConnectionStatus.Connected ? disconnectedRate : connectedRate);
				
				gRequest.Abort ();
				aRequest.Abort ();
				errorMessage = String.Empty;
			}
		}
		
		
		/// <summary>
		/// Sets the refresh rates (seconds) which the tool will perform the connection check.
		/// </summary>
		/// <param name="connected">Sets the default rate when the device has been marked as connected in the previous iteration.</param>
		/// <param name="disconnected">Sets the default rate when the device has been marked as disconnected in the previous iteration. If value is empty or lower than zero, the connected parameter will be considered default for both situations.</param>
		public static void SetRefreshRates (int connected, int disconnected = -1) {
			if (connected > 0) {
				connectedRate = connected * 1000;
			}
			
			if (disconnected > 0) {
				disconnectedRate = disconnected * 1000;
			} else if (connected > 0) {
				disconnectedRate = connected * 1000;
			}
		}
		
		
		/// <summary>
		/// Sets a website to perform a connection test, which will determine the server status.
		/// </summary>
		public static void SetServerWebsite (string website) { 
			currentWebsite = website;
		}
	}
	#endregion
	
	#region Ping Connection Test
	public static class PingConnectionTest {
		
		static PingConnection pingConnection;
		
		public static ConnectionStatus CurrentStatus 
		{
			get
			{
				if (pingConnection != null) {
					return pingConnection.CurrentStatus;
				} else {
					return ConnectionStatus.NotRunning;
				}
			}
		}
		
		public static float PingTime 
		{
			get
			{
				if (pingConnection != null) {
					return pingConnection.pingTime;
				} else {
					return -1;
				}
			}
		}
		
		/// <summary>
		/// Starts the connection check by pinging.
		/// </summary>
		/// <param name="address">Address to be pinged. The address should be a simple web addres such as "google.com" or "farofastudios.com.br", or a valid IPv4 address such as "141.0.174.41".</param>
		public static void StartConnectionCheck (string address = default(string)) {
			if (pingConnection == null) {
				pingConnection = new GameObject ("Ping Connection Helper").AddComponent<PingConnection> ();
				pingConnection.Init (address);
				pingConnection.gameObject.hideFlags = HideFlags.HideInHierarchy;
			} else {
				pingConnection.serverHost = Dns.GetHostEntry (address).AddressList [0].ToString ();
			}
		}
		
		/// <summary>
		/// Sets the refresh rates (seconds) which the tool will perform the connection check.
		/// </summary>
		/// <param name="connected">Sets the default rate when the device has been marked as connected in the previous iteration.</param>
		/// <param name="disconnected">Sets the default rate when the device has been marked as disconnected in the previous iteration. If value is empty or lower than zero, the connected parameter will be considered default for both situations.</param>
		public static void SetRefreshRates (float connected, float disconnected = -1) {
			if (connected > 0) {
				pingConnection.connectedRate = connected;
			}
			
			if (disconnected > 0) {
				pingConnection.disconnectedRate = disconnected;
			} else if (connected > 0) {
				pingConnection.disconnectedRate = connected;
			}
		}
		
		
		/// <summary>
		/// Sets a website to perform a connection test, which will determine the server status.
		/// </summary>
		public static void SetServerAddress (string address) { 
			if (!string.IsNullOrEmpty (address)) {
				try {
					pingConnection.serverHost = Dns.GetHostEntry (address).AddressList [0].ToString ();
				} catch {
					Debug.LogError ("Please check the address given as ping check.");
				}
			}
		}
	}
	
	class PingConnection : MonoBehaviour {
		
		public Coroutine connectionRoutine;
		
		public float connectedRate = 10f;
		public float disconnectedRate = 1f;
		
		public string serverHost;
		public string googleHost;
		public ConnectionStatus CurrentStatus = ConnectionStatus.NotRunning;
		
		public int pingTime;
		
		public void Init (string address = default(string)) {
			googleHost = Dns.GetHostEntry ("google.com").AddressList [0].ToString ();
			
			if (!string.IsNullOrEmpty (address)) {
				try {
					serverHost = Dns.GetHostEntry (address).AddressList [0].ToString ();
				} catch {
					Debug.LogError ("Please check the address given as ping check.");
				}
			}
			
			connectionRoutine = StartCoroutine(ConnectionCheck());
		}
		
		public IEnumerator ConnectionCheck()
		{
			CurrentStatus = ConnectionStatus.Connecting;
			
			while (true) {
				Ping gRequest = new Ping(googleHost);
				
				while (!gRequest.isDone) 
				{
					yield return null;
				}
				
				if (gRequest.time >= 0) {
					if (!string.IsNullOrEmpty (serverHost)) {
						Ping aRequest = new Ping (serverHost);
						
						while (!aRequest.isDone) {
							yield return null;
						}
						
						if (aRequest.time >= 0) {
							pingTime = aRequest.time;
							CurrentStatus = ConnectionStatus.Connected;
						} else {
							Debug.LogWarning ("The given addres did not answer the ping.");
							CurrentStatus = ConnectionStatus.WebServersDown;
						}
					} else {
						Debug.LogWarning ("No address given as server.");
					}
				} 
				else 
				{
					Debug.LogWarning ("Not connected to the internet.");
					CurrentStatus = ConnectionStatus.NoConnection;	
				}
				
				if (CurrentStatus != ConnectionStatus.Connected)
					pingTime = -1;
				
				yield return new WaitForSeconds (CurrentStatus != ConnectionStatus.Connected ? disconnectedRate : connectedRate);
			}
		}
	}
	#endregion
}