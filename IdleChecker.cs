﻿using UnityEngine;
using System.Linq;
using System.Collections;

/// <summary>
/// Checa que o usuário está um tempo sem mecher. Caso esteja ele chama o método informado.
/// Esse objeto não é destruído quando a cena é recarregada, mas só pode haver um.
/// </summary>
public class IdleChecker : MonoBehaviour {
	[SerializeField] string sendMessageMethod;
	[SerializeField] string[] ignoreScenes;
	[SerializeField] float idleTimeInSeconds;

	internal int createdFrameCount = 0; // Variável utilizada para garantir a idade do objeto

	private float AutomaticSyncTimer;

	void Start () {
		createdFrameCount = Time.frameCount;
		bool foundOlder = false;
		foreach(IdleChecker idlechecker in FindObjectsOfType<IdleChecker>()){
			if(createdFrameCount>idlechecker.createdFrameCount){
				foundOlder = true;
				break;
			}
		}
		// Se autodestrói se existir outro mais velho ou igual
		if(foundOlder){
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad(gameObject);

		AutomaticSyncTimer = Time.time;
	}

	void Update(){
		if (Input.GetMouseButton (0) || Input.touches.Length > 0) {
			AutomaticSyncTimer = Time.time;
		}
		if (Time.time - AutomaticSyncTimer > idleTimeInSeconds) {
			AutomaticSyncTimer = Time.time;
			if (!ignoreScenes.Contains (Application.loadedLevelName)) {
				SendMessage(sendMessageMethod);
			}
		}
	}
}