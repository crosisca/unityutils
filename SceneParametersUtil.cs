﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Uma classe que serve para a troca de parâmetros entre cenas. Ela garante que o acesso às variáveis só pode ser feito pela
/// cena que foi chamada e garante que se os parâmetros não forem utilizados, eles não ficam na memória.
/// <example>
/// A classe pode ser usada fazendo uma chamada para o método "LoadLevel" da seguinte forma:
///		SceneParametersUtil.LoadLevel("NomeDaCena","chave1",true,"chave2",100);
/// Depois é possível acessar os valores na nova cena utilizando o método "GetParameters()":
///		Dictionary&lt;string, object&gt; parameters = SceneParametersUtil.GetParameters();
///		bool chave1 = (bool)parameters["chave1"];
///		int chave2 = (int)parameters["chave2"];
///	É importante que o cast seja feito para o tipo correto, já que a classe não guarda nenhum indicador de tipo.
/// </example>
/// <remarks>
/// Quando é feita a troca de cena, um GameObject é criado com um script que espera pelo fim do primeiro frame e depois limpa a lista de parâmetros,
/// esse script roda depois de todos os Starts da cena.
/// </remarks>
/// <remarks>
/// Depois que você chama o método "GetParameters" a lista de parâmetros é limpa, e subsequentes chamadas retornam um dicionário vazio.
/// </remarks>
/// </summary>
public static class SceneParametersUtil
{

    /// <summary>
    /// Uma classe de ajuda que garante que ao trocar de cena, os parâmetros passados, se não forem utilizados, são jogados fora.
    /// </summary>
    public class SceneParameterCleaner : MonoBehaviour
    {
        void OnLevelWasLoaded(int index)
        {
            StartCoroutine(LevelWasLoaded());
        }

        IEnumerator LevelWasLoaded()
        {
            yield return new WaitForEndOfFrame();
            SceneParametersUtil.Clear();
            Destroy(gameObject);
        }
    }

    private static Dictionary<string, object> _parameters = new Dictionary<string, object>();

    /// <summary>
    /// Carrega uma cena passando parâmetros para ela.
    /// </summary>
    /// <param name="sceneName">O nome da cena a ser carregada</param>
    /// <param name="parameters">Os parâmetros que a cena vai receber, deve seguir a estrutura {(string)chave, (object)valor[, ...]}</param>
    public static void LoadLevel(string sceneName, params object[] parameters)
    {
        _SetParameters(parameters);
        Object.DontDestroyOnLoad(new GameObject("SceneParameterCleaner", typeof(SceneParameterCleaner)));
        Application.LoadLevel(sceneName);
    }

    /// <summary>
    /// Carrega uma cena passando parâmetros para ela.
    /// </summary>
    /// <param name="sceneName">O index da cena a ser carregada</param>
    /// <param name="parameters">Os parâmetros que a cena vai receber, deve seguir a estrutura {(string)chave, (object)valor[, ...]}</param>
    public static void LoadLevel(int index, params object[] parameters)
    {
        _SetParameters(parameters);
        Object.DontDestroyOnLoad(new GameObject("SceneParameterCleaner", typeof(SceneParameterCleaner)));
        Application.LoadLevel(index);
    }

    /// <summary>
    /// Retorna uma cópia da lista de parâmetros e apaga os parâmetros anteriores.
    /// </summary>
    /// <returns>A lista de parâmetros atual.</returns>
    public static Dictionary<string, object> GetParameters()
    {
        var ret = new Dictionary<string, object>(_parameters);
        //Clear();
        return ret;
    }

    /// <summary>
    /// Limpa a lista de parâmetros.
    /// </summary>
    public static void Clear()
    {
        _parameters.Clear();
    }

    private static void _SetParameters(params object[] parameters)
    {
        if (parameters == null)
            return;

        if (parameters.Length % 2 != 0)
            throw (new System.Exception("Você precisa passar um número par de parâmetros, no formato chave:valor"));

        Clear();
        for (int i = 0; i < parameters.Length; i += 2)
        {
            _parameters[parameters[i].ToString()] = parameters[i + 1];
        }
    }

    [System.Obsolete("Somente pra debug, nao buildar usando esse metodo")]
    public static void DebugSetParameters(params object[] parameters)
    {
        if (parameters.Length % 2 != 0)
            throw (new System.Exception("Você precisa passar um número par de parâmetros, no formato chave:valor"));

        Clear();
        for (int i = 0; i < parameters.Length; i += 2)
        {
            _parameters[parameters[i].ToString()] = parameters[i + 1];
        }
    }
}