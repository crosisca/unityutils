﻿/*  ***********************
//  * COPYRIGHT DO ARTHUR *
//  ***********************
// 99 USD CADA LICENÇA
// 1 LICENÇA POR COMPUTADOR
*/
using System.Reflection;
using UnityEditor;
using UnityEngine;

public static class NGUIAnchorTools
{
	#if UNITY_EDITOR_OSX
	[MenuItem ("NGUI/Anchors Tools/Anchor To Current Position &a", false, 30)]
	#elif UNITY_EDITOR_WIN
	[MenuItem ("NGUI/Anchors Tools/Anchor To Current Position %&#a", false, 30)]	
	#endif
	public static void ToCurrentPositionAll()
	{
		ToCurrentPosition(false);
	}

	[MenuItem ("NGUI/Anchors Tools/Refresh Existing Anchors", false, 30)]
	public static void ToCurrentPositionIgnoringUnanchored()
	{
		ToCurrentPosition(true);
	}

	static void ToCurrentPosition(bool ignoreUnanchored)
	{
		foreach(GameObject selectedObj in Selection.gameObjects)
		{
			UIRect rect = selectedObj.GetComponent<UIRect>();

			if(rect == null)
				continue;
			
			UIRect.AnchorPoint[] selectedRectAnchors = new UIRect.AnchorPoint[]{rect.leftAnchor, rect.topAnchor, rect.rightAnchor, rect.bottomAnchor};
			
			if(!ignoreUnanchored && !rect.isAnchored)
			{
				UIRect parent = NGUITools.FindInParents<UIRect>(rect.cachedTransform.parent);	
				
				if (parent != null)
				{
					for (int i = 0; i < selectedRectAnchors.Length; i++)
					{	
						selectedRectAnchors[i].target = parent.cachedTransform;

						if(i%2==0)
							UIRectEditor.UpdateHorizontalAnchor(rect, selectedRectAnchors[i], true);
						else
							UIRectEditor.UpdateVerticalAnchor(rect, selectedRectAnchors[i], true);
					}
				}
			}
			
			for(int i=0; i < selectedRectAnchors.Length; i++)
			{
				if(selectedRectAnchors[i].target == null)
					continue;				
				
				Vector3[] sides = selectedRectAnchors[i].target.GetComponent<UIRect>().GetSides(rect.cachedTransform);
				
				bool isHorizontal = (i % 2 == 0);

				float f0, f1;
				
				if(isHorizontal)
				{
					f0 = sides[0].x;
					f1 = sides[2].x;
				}
				else
				{
					f0 = sides[3].y;
					f1 = sides[1].y;
				}
				
				float final = Mathf.Floor(0.5f + Mathf.Lerp(0f, f1 - f0, selectedRectAnchors[i].relative) + selectedRectAnchors[i].absolute);
				
				selectedRectAnchors[i].relative = final / (f1 - f0);
				selectedRectAnchors[i].absolute = 0;
			}
			rect.ResetAndUpdateAnchors();
			RepaintUIRectEditor ();
		}
	}
	
	#if UNITY_EDITOR_OSX
	[MenuItem ("NGUI/Anchors Tools/Unanchor &u", false, 30)]
	#elif UNITY_EDITOR_WIN
	[MenuItem ("NGUI/Anchors Tools/Unanchor #u", false, 30)]
	#endif
	public static void Unanchor()
	{
		foreach(GameObject selectedObj in Selection.gameObjects)
		{
			UIRect rect = selectedObj.GetComponent<UIRect>();
			if(rect != null)
			{
				rect.leftAnchor.target = null;
				rect.topAnchor.target = null;
				rect.rightAnchor.target = null;
				rect.bottomAnchor.target = null;
			}
			RepaintUIRectEditor ();
		}
	}
	
	#if UNITY_EDITOR_OSX
	[MenuItem ("NGUI/Anchors Tools/Based on Height &h", false, 30)]
	#elif UNITY_EDITOR_WIN
	[MenuItem ("NGUI/Anchors Tools/Based on Height #h", false, 30)]
	#endif
	public static void BasedOnHeight()
	{
		foreach(GameObject selectedObj in Selection.gameObjects)
		{
			UIWidget widget = selectedObj.GetComponent<UIWidget>();
			if(widget != null)
			{
				widget.keepAspectRatio = UIWidget.AspectRatioSource.BasedOnHeight;
			}
		}
	}

	#if UNITY_EDITOR_OSX
	[MenuItem("NGUI/Anchors Tools/Based on Width &w", false, 30)]
	#elif UNITY_EDITOR_WIN
	[MenuItem("NGUI/Anchors Tools/Based on Width #w", false, 30)]
	#endif
    public static void BasedOnWidth()
    {
        foreach (GameObject selectedObj in Selection.gameObjects)
        {
            UIWidget widget = selectedObj.GetComponent<UIWidget>();
            if (widget != null)
            {
                widget.keepAspectRatio = UIWidget.AspectRatioSource.BasedOnWidth ;
			}
        }
    }
	
	#if UNITY_EDITOR_OSX
	[MenuItem ("NGUI/Anchors Tools/Free Aspect Ratio &f", false, 30)]
	#elif UNITY_EDITOR_WIN
	[MenuItem ("NGUI/Anchors Tools/Free Aspect Ratio %#f", false, 30)]
	#endif
	public static void FreeAspect()
	{
		foreach(GameObject selectedObj in Selection.gameObjects)
		{
			UIWidget widget = selectedObj.GetComponent<UIWidget>();
			if(widget != null)
			{
				widget.keepAspectRatio = UIWidget.AspectRatioSource.Free;
			}
		}
	}
		
	public static void RepaintUIRectEditor()
	{
		BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
		typeof(UIRectEditor).GetMethod("ReEvaluateAnchorType", flags).Invoke (UIRectEditor.instance, null);
	}
}