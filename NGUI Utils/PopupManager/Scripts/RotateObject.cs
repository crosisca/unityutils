﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {
    
    [SerializeField]
    float speed = 50;

	// Update is called once per frame
	void Update () {
        transform.Rotate(0, 0, -speed * Time.deltaTime);
	}
}
