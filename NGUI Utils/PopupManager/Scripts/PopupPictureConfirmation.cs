﻿using UnityEngine;
using System.Collections;


public class PopupPictureConfirmation : MonoBehaviour
{
	UIButton buttonConfirm;
	UIButton buttonCancel;

	public delegate void PopupPictureConfirmationCallback();
	public event PopupPictureConfirmationCallback OnConfirm;
	public event PopupPictureConfirmationCallback OnCancel;

	void Start()
	{
		buttonConfirm = transform.FindChild("ButtonConfirm").GetComponent<UIButton>();
		buttonConfirm.onClick.Add(new EventDelegate(this, "OnClickConfirm"));

		buttonCancel = transform.FindChild("ButtonCancel").GetComponent<UIButton>(); ;
		buttonCancel.onClick.Add(new EventDelegate(this, "OnClickCancel"));
	}

	public void SetInfo(Texture2D picture)
	{
		transform.FindChild("Picture Container/Picture").GetComponent<UITexture>().mainTexture = picture;
	}

	void OnClickConfirm()
	{
		if (OnConfirm != null)
			OnConfirm();

		Destroy(gameObject);
	}

	void OnClickCancel()
	{
		if (OnCancel != null)
			OnCancel();

		Destroy(gameObject);
	}

	void OnDestroy()
    {
        if (OnConfirm != null)
            foreach (PopupPictureConfirmationCallback subscriber in OnConfirm.GetInvocationList())
                OnConfirm -= subscriber;
        buttonConfirm.onClick.Remove(new EventDelegate(this, "OnClickConfirm"));

        if (OnCancel != null)
            foreach (PopupPictureConfirmationCallback subscriber in OnCancel.GetInvocationList())
                OnCancel -= subscriber;
        buttonCancel.onClick.Remove(new EventDelegate(this, "OnClickCancel"));
    }
}