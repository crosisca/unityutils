﻿using UnityEngine;
using System.Collections;

public class PopupLoadingMessage : MonoBehaviour
{
    public void SetInfo(string msg)
    {
        transform.FindChild("Message").GetComponent<UILabel>().text = msg;
    }
}