﻿using UnityEngine;
using System.Collections;

public class PopupConfirmation : MonoBehaviour
{
    UIButton buttonConfirm;
    UIButton buttonCancel;

    public delegate void PopupConfirmationCallback();
    public event PopupConfirmationCallback OnConfirm;
    public event PopupConfirmationCallback OnCancel;

    void Start()
    {
        buttonConfirm = transform.FindChild("ButtonConfirm").GetComponent<UIButton>();
        buttonConfirm.onClick.Add(new EventDelegate(this, "OnClickConfirm"));

        buttonCancel = transform.FindChild("ButtonCancel").GetComponent<UIButton>(); ;
        buttonCancel.onClick.Add(new EventDelegate(this, "OnClickCancel"));
    }

    public void SetInfo(string title, string body, string buttonConfirmText, string buttonCancelText)
    {
        transform.FindChild("Title").GetComponent<UILabel>().text = title;
        transform.FindChild("Message").GetComponent<UILabel>().text = body;
        transform.FindChild("ButtonConfirm").GetComponentInChildren<UILabel>().text = buttonConfirmText;
        transform.FindChild("ButtonCancel").GetComponentInChildren<UILabel>().text = buttonCancelText;
    }

    void OnClickConfirm()
    {
        if (OnConfirm != null)
            OnConfirm();

        Destroy(gameObject);
    }

    void OnClickCancel()
    {
        if (OnCancel != null)
            OnCancel();

        Destroy(gameObject);
    }

    void OnDestroy()
    {
        if (OnConfirm != null)
            foreach (PopupConfirmationCallback subscriber in OnConfirm.GetInvocationList())
                OnConfirm -= subscriber;
        buttonConfirm.onClick.Remove(new EventDelegate(this, "OnClickConfirm"));

        if (OnCancel != null)
            foreach (PopupConfirmationCallback subscriber in OnCancel.GetInvocationList())
                OnCancel -= subscriber;
        buttonCancel.onClick.Remove(new EventDelegate(this, "OnClickCancel"));
    }
}