﻿using UnityEngine;
using System.Collections;

public class PopupHandler
{
    public static GameObject ShowLoading()
    {
        CloseAll();

        GameObject root = GameObject.Find("UI Root");
		GameObject popupLoading;

		if(root != null)
			popupLoading = NGUITools.AddChild(root.transform.FindChild("Camera").gameObject, Resources.Load("PopupLoading") as GameObject);
		else
			popupLoading = NGUITools.AddChild(Object.FindObjectOfType <UICamera>().gameObject, Resources.Load("PopupLoading") as GameObject);			
		
        return popupLoading;
    }

    public static GameObject ShowLoadingMessage(string msg)
    {
        CloseAll();

        GameObject root = GameObject.Find("UI Root");
        GameObject popupLoading = NGUITools.AddChild(root.transform.FindChild("Camera").gameObject, Resources.Load("PopupLoadingMessage") as GameObject);
        popupLoading.GetComponent<PopupLoadingMessage>().SetInfo(msg);
        return popupLoading;
    }

    public static GameObject ShowMessage(string msg)
    {
        CloseAll();

        GameObject root = GameObject.Find("UI Root");
        GameObject popupMessage = NGUITools.AddChild(root.transform.FindChild("Camera").gameObject, Resources.Load("PopupMessage") as GameObject);
        popupMessage.GetComponent<PopupMessage>().SetInfo(msg);
        return popupMessage;
    }

    public static GameObject ShowWarning(string title, string body, string buttonText, PopupWarning.PopupWarningCallback callback = null)
    {
        CloseAll();

        GameObject root = GameObject.Find("UI Root");
        GameObject popupWarning = NGUITools.AddChild(root.transform.FindChild("Camera").gameObject, Resources.Load("PopupWarning") as GameObject);
        popupWarning.GetComponent<PopupWarning>().SetInfo(title, body, buttonText);
        if(callback != null)
            popupWarning.GetComponent<PopupWarning>().OnConfirm += callback;
        return popupWarning;
    }

    public static GameObject ShowConfirmation(string title,string body,string buttonConfirmText,string buttonCancelText, PopupConfirmation.PopupConfirmationCallback confirmCallback = null, PopupConfirmation.PopupConfirmationCallback cancelCallback = null)
    {
        CloseAll();

        GameObject root = GameObject.Find("UI Root");
        GameObject popupConfirmation = NGUITools.AddChild(root.transform.FindChild("Camera").gameObject, Resources.Load("PopupConfirmation") as GameObject);
        popupConfirmation.GetComponent<PopupConfirmation>().SetInfo(title, body, buttonConfirmText, buttonCancelText);
        if(confirmCallback != null)
            popupConfirmation.GetComponent<PopupConfirmation>().OnConfirm += confirmCallback;
        if(cancelCallback != null)
            popupConfirmation.GetComponent<PopupConfirmation>().OnCancel += cancelCallback;
        return popupConfirmation;
    }

	public static GameObject ShowPictureConfirmation(Texture2D picture, PopupPictureConfirmation.PopupPictureConfirmationCallback confirmCallback = null, PopupPictureConfirmation.PopupPictureConfirmationCallback cancelCallback = null)
	{
		CloseAll();

		GameObject root = GameObject.Find("UI Root");
		GameObject popupConfirmation = NGUITools.AddChild(root.transform.FindChild("Camera").gameObject, Resources.Load("PopupPictureConfirmation") as GameObject);
		popupConfirmation.GetComponent<PopupPictureConfirmation>().SetInfo(picture);
		if(confirmCallback != null)
			popupConfirmation.GetComponent<PopupPictureConfirmation>().OnConfirm += confirmCallback;
		if(cancelCallback != null)
			popupConfirmation.GetComponent<PopupPictureConfirmation>().OnCancel += cancelCallback;
		return popupConfirmation;
	}
    
    public static void CloseAll()
    {
        if (GameObject.FindWithTag("Popup") != null)
            GameObject.Destroy(GameObject.FindWithTag("Popup"));
    }
}