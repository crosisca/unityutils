﻿using UnityEngine;
using System.Collections;

public class PopupWarning : MonoBehaviour 
{
    UIButton buttonOK;

    public delegate void PopupWarningCallback();
    public event PopupWarningCallback OnConfirm;

    void Start()
    {
        buttonOK = GetComponentInChildren<UIButton>();
        buttonOK.onClick.Add(new EventDelegate(this, "OnClickOK"));
    }

    public void SetInfo(string title, string body, string buttonText)
    {
        transform.FindChild("Title").GetComponent<UILabel>().text = title;
        transform.FindChild("Message").GetComponent<UILabel>().text = body;
        transform.FindChild("ButtonOK").GetComponentInChildren<UILabel>().text = buttonText;
    }

    void OnClickOK()
    {
        if(OnConfirm != null)
            OnConfirm();

        Destroy(gameObject);
    }

    void OnDestroy()
    {
        if (OnConfirm != null)
            foreach (PopupWarningCallback subscriber in OnConfirm.GetInvocationList())
                OnConfirm -= subscriber;
        buttonOK.onClick.Remove(new EventDelegate(this, "OnClickOK"));
    }

}
