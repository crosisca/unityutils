﻿using UnityEngine;
using System.Collections;

public class PopupMessage : MonoBehaviour
{
    public void SetInfo(string msg)
    {
        transform.FindChild("Message").GetComponent<UILabel>().text = msg;
    }
}