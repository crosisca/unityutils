﻿using UnityEngine;
using System.Collections;
using System;

public class AlertPopup : MonoBehaviour
{
	public event AlertManager.AlertEventHandler alertClicked;
	public event AlertManager.AlertEventHandler alertFinished;

	public UILabel title;
	public UILabel text;
	public UITexture icon;

	Vector2 initPos;

	public void Play(int secondsDisplayed)
	{
		StartCoroutine (SetInitPos(secondsDisplayed));
	}

	IEnumerator SetInitPos(int secondsDisplayed)
	{
		yield return  null; //Espera as ancoras
		initPos = new Vector2(transform.localPosition.x , 640 + GetComponent <UIWidget>().height);
		transform.localPosition = initPos;
		StartCoroutine (Tween(true, secondsDisplayed));
	}

	void OnClick()
	{
		StopAllCoroutines ();
		if(alertClicked != null)
			alertClicked ();
		StartCoroutine (Tween (false));
	}

	IEnumerator Tween(bool forward, int secondsDisplayed = 0)
	{
		Vector2 desiredPos;
		Predicate<Transform> condition;

		if(forward)
		{
			desiredPos = new Vector2 (transform.localPosition.x, 640);
			condition = x => x.localPosition.y > desiredPos.y;
		}
		else
		{
			desiredPos = initPos;	
			condition = x => x.localPosition.y < desiredPos.y;
		}

		while(condition(transform))
		{
			transform.localPosition = Vector2.MoveTowards (transform.localPosition, desiredPos, 3);
			yield return null;
		}
			
		if(forward)
		{
			yield return new WaitForSeconds (secondsDisplayed);
			StartCoroutine (Tween (false));
		}
		else
		{
			alertFinished ();
			Destroy (gameObject);
		}
	}


}
