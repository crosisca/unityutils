﻿using UnityEngine;
using System.Collections.Generic;

public static class AlertManager 
{
	public delegate void AlertEventHandler();

	class Alert
	{
		public string title;
		public string text;
		public string iconName;
		public int secondsDisplayed;
		public AlertEventHandler alertClickedCallback;

		public Alert(string title, string text, string iconName, int secondsDisplayed, AlertEventHandler alertClickedCallback = null)
		{
			this.title = title;
			this.text = text;
			this.iconName = iconName;
			this.secondsDisplayed = secondsDisplayed;
			this.alertClickedCallback = alertClickedCallback;
		}
	}

	static readonly List<Alert> alertQueue = new List<Alert>();
	static bool alertActiveOnScreen;

	public static void NewAlert(string title, string text, string iconName, int secondsDisplayed, AlertEventHandler alertClickedCallback = null)
	{
		alertQueue.Add (new Alert(title, text, iconName, secondsDisplayed, alertClickedCallback));
		TryToSendNextAlert ();
	}

	static void TryToSendNextAlert()
	{
		if (!alertActiveOnScreen && alertQueue.Count > 0)
		{
			ShowAlert (alertQueue [0]);			
		}
	}

	static void ShowAlert(Alert alert)
	{
		alertActiveOnScreen = true;

		GameObject alertPrefab = NGUITools.AddChild (Object.FindObjectOfType  <UICamera> ().gameObject, Resources.Load<GameObject> ("AlertPopup"));
		alertPrefab.transform.localPosition = Vector2.zero;
		NGUITools.AddWidgetCollider(alertPrefab);

		UIRect alertRect = alertPrefab.GetComponent <UIRect> ();
		Transform parentPanel = alertRect.GetComponentInParent <UIPanel> ().transform;

		alertRect.rightAnchor.target = parentPanel;
		alertRect.rightAnchor.relative = 0;
		alertRect.rightAnchor.absolute = 0;

		alertRect.leftAnchor.target = parentPanel;
		alertRect.rightAnchor.relative = 1;
		alertRect.rightAnchor.absolute = 0;

		alertRect.bottomAnchor.target = alertRect.transform.FindChild ("Message");
		alertRect.bottomAnchor.relative = 0;
		alertRect.bottomAnchor.absolute = -30;

		alertRect.ResetAndUpdateAnchors ();

		AlertPopup popup = alertPrefab.GetComponent <AlertPopup>();
		popup.title.text = alert.title;
		popup.text.text = alert.text;
		popup.icon.mainTexture = Resources.Load <Texture2D> ("Icones/" + alert.iconName);

		if (alert.alertClickedCallback != null) 
		{
			popup.alertClicked += alert.alertClickedCallback;
		}
		popup.alertFinished += AlertFinishedListener;

		popup.Play (alert.secondsDisplayed);
	}

	static void AlertFinishedListener()
	{
		alertQueue [0] = null;
		alertQueue.RemoveAt (0);
		alertActiveOnScreen = false;
		TryToSendNextAlert ();
	}
}
