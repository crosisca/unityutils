﻿using UnityEngine;
using System.Collections;

public class FollowDrag : MonoBehaviour {

    [SerializeField]
    public UIScrollView horizontalLinkedScrollView, verticalLinkedScrollView;
    bool shouldUpdate;
    UIScrollView scrollView;
    UIPanel panel, linkedVerticalPanel, linkedHorizontalPanel;

    UIScrollView.Movement desiredMovement;

    [SerializeField]
    bool followHorizontal = false, followVertical = true;

	void OnEnable () {
        scrollView = GetComponent<UIScrollView>();
        if (scrollView == null)
            return;

        panel = GetComponent<UIPanel>();

        if(verticalLinkedScrollView != null)
        {
            linkedVerticalPanel = verticalLinkedScrollView.GetComponent<UIPanel>();
            verticalLinkedScrollView.onDragStarted += OnLinkedDragStarted;
        }
        if(horizontalLinkedScrollView != null)
        {
            linkedHorizontalPanel = horizontalLinkedScrollView.GetComponent<UIPanel>();
            horizontalLinkedScrollView.onDragStarted += OnLinkedDragStarted;
        }

        scrollView.onDragStarted += OnDragStarted;
        desiredMovement = scrollView.movement;
	}

    void OnDragStarted()
    {
        shouldUpdate = false;
    }

    void OnLinkedDragStarted()
    {
        shouldUpdate = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (scrollView == null || panel == null)
        {
            Debug.LogWarning("Nao foi possivel encontrar a scrollview ou o painel desse objeto.");
            return;
        }

        bool h = true;
        bool v = true;

        if(desiredMovement == UIScrollView.Movement.Unrestricted)
        {
            h = true;
            v = true;
        }
        else if(desiredMovement == UIScrollView.Movement.Horizontal)
        {
            h = true;
            v = false;
        }
        else if(desiredMovement == UIScrollView.Movement.Vertical)
        {
            h = false;
            v = true;
        }
        if (horizontalLinkedScrollView != null)
            if (horizontalLinkedScrollView.disableDragIfFits && !horizontalLinkedScrollView.shouldMoveHorizontally)
                h = false;

        if (verticalLinkedScrollView != null)
            if (verticalLinkedScrollView.disableDragIfFits && !verticalLinkedScrollView.shouldMoveVertically)
                    v = false;

        if (h && v)
            scrollView.movement = desiredMovement;
        else if (h && !v)
            scrollView.movement = UIScrollView.Movement.Horizontal;
        else if (!h && v)
            scrollView.movement = UIScrollView.Movement.Vertical;





       
        if (shouldUpdate)
        {
            if (followVertical && verticalLinkedScrollView != null)
            {
                scrollView.transform.localPosition = new Vector3(scrollView.transform.localPosition.x, verticalLinkedScrollView.transform.localPosition.y, scrollView.transform.localPosition.z);
                panel.clipOffset = new Vector2(panel.clipOffset.x, linkedVerticalPanel.clipOffset.y);
            }
            if (followHorizontal && horizontalLinkedScrollView != null)
            {
                scrollView.transform.localPosition = new Vector3(horizontalLinkedScrollView.transform.localPosition.x, scrollView.transform.localPosition.y, scrollView.transform.localPosition.z);
                panel.clipOffset = new Vector2(linkedHorizontalPanel.clipOffset.x, panel.clipOffset.y);
            }
        }

	}

    void OnDisable()
    {
        scrollView.onDragStarted -= OnDragStarted;
        if (verticalLinkedScrollView != null)
            verticalLinkedScrollView.onDragStarted -= OnLinkedDragStarted;

        if (horizontalLinkedScrollView != null)
            horizontalLinkedScrollView.onDragStarted -= OnLinkedDragStarted;
    }
}
