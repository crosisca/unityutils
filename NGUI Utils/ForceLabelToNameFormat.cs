﻿using UnityEngine;
using System.Collections;
using System.Globalization;

public class ForceLabelToNameFormat : MonoBehaviour {

    UILabel label;
    bool hasListener = false;
	// Use this for initialization
	void Start () {
        label = GetComponent<UILabel>();
        if(label != null && !hasListener)
        {
            label.onChange += OnLabelChange;
            hasListener = true;
        }
	}

    void OnEnable ()
    {
        if (label != null && !hasListener)
        {
            label.onChange += OnLabelChange;
            hasListener = true;
        }
    }

    void OnDisable ()
    {
        if (label != null && hasListener)
        {
            label.onChange -= OnLabelChange;
            hasListener = false;
        }
    }
	
    void OnLabelChange()
    {
        label.text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(label.text.ToLower());
    }
}
