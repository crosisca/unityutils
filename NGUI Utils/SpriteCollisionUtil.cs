﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SpriteCollisionUtil
{

    public static bool ClickedOnSprite(UISprite sprite)
    {
        Vector2 screenTouchPos = UICamera.currentTouch.pos;
        Vector3 spritePos = UICamera.mainCamera.WorldToScreenPoint(sprite.worldCenter);
        Vector2 spriteTouchPos = new Vector2(screenTouchPos.x - spritePos.x, screenTouchPos.y - spritePos.y);

        List<Vector3> spriteFillVertices = sprite.geometry.verts.ToArray().ToList();
        int qtd = Mathf.FloorToInt((float)spriteFillVertices.Count / 4f);

        RotateVerts(spriteFillVertices, sprite.transform.localRotation.eulerAngles.z);

        bool clickedOnSlice = false;
        for (int i = 0; i < qtd; i++)
        {
            Vector2[] polygon = new Vector2[4];
            polygon[0] = new Vector2(spriteFillVertices[i * 4].x, spriteFillVertices[i * 4].y);
            polygon[1] = new Vector2(spriteFillVertices[i * 4 + 1].x, spriteFillVertices[i * 4 + 1].y);
            polygon[2] = new Vector2(spriteFillVertices[i * 4 + 2].x, spriteFillVertices[i * 4 + 2].y);
            polygon[3] = new Vector2(spriteFillVertices[i * 4 + 3].x, spriteFillVertices[i * 4 + 3].y);

            float[] listX = { polygon[0].x, polygon[1].x, polygon[2].x, polygon[3].x };
            float[] listY = { polygon[0].y, polygon[1].y, polygon[2].y, polygon[3].y };

            if (isPointInPolygon(polygon.Length, listX, listY, spriteTouchPos))
            {
                clickedOnSlice = true;
                break;
            }
        }
        return clickedOnSlice;
    }

    static bool isPointInPolygon(int nvert, float[] vertx, float[] verty, Vector2 point)
    {
        float testx = point.x;
        float testy = point.y;
        int i, j = 0;
        bool c = false;
        for (i = 0, j = nvert - 1; i < nvert; j = i++)
        {
            if (((verty[i] > testy) != (verty[j] > testy)) &&
             (testx < (vertx[j] - vertx[i]) * (testy - verty[i]) / (verty[j] - verty[i]) + vertx[i]))
                c = !c;
        }
        return c;
    }

    static void RotateVerts(List<Vector3> polygonVerts, float angle)
    {
        for (int i = 0; i < polygonVerts.Count; i++)
        {
            polygonVerts[i] = RotatePoint(polygonVerts[i], angle);
        }
    }

    static Vector3 RotatePoint(Vector3 point, float angle)
    {
        float xnew = point.x * Mathf.Cos(ToRadians(angle)) - point.y * Mathf.Sin(ToRadians(angle));
        float ynew = point.x * Mathf.Sin(ToRadians(angle)) + point.y * Mathf.Cos(ToRadians(angle));

        return new Vector3(xnew, ynew, point.z);
    }

    static float ToRadians(float val)
    {
        return (Mathf.PI / 180) * val;
    }

}
