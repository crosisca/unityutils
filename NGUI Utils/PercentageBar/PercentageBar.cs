using UnityEngine;
using System.Collections;

/// <summary>
/// Barra de porcentagem. Enquanto pressiona ela o valor máximo e mínimo são exibidos. Setup inicializa ela
/// </summary>
[RequireComponent (typeof(UISliderCustom))]
public class PercentageBar : MonoBehaviour {
    int _total;
	int _current;
	Color _percentageFillColor;
    
	UISliderCustom percentageBar;
    UILabel percentageLabel;
	UISprite percentageFillSprite;

    void Start(){
		if(percentageBar==null) // Força uma inicialização se ainda não foi inicializado
			Initialize();
    }
    
    public void Initialize(){
		percentageBar = GetComponent<UISliderCustom>();
        percentageLabel = percentageBar.GetComponentInChildren<UILabel>();
	}

	void OnEnable(){
		SetPercentageFillColor();
	}
	
	public void SetPercentageFillColor(Color color){
		_percentageFillColor = color;
		SetPercentageFillColor();
	}
	
	void SetPercentageFillColor(){
		if(percentageFillSprite==null && percentageBar!=null)
			percentageFillSprite = percentageBar.foregroundWidget.GetComponent<UISprite>();
		if(percentageFillSprite!=null && percentageFillSprite.color!=_percentageFillColor){
			percentageFillSprite.color=_percentageFillColor;
		}
	}

	public void Setup(int current, int total){
		if(percentageBar==null) // Força uma inicialização se ainda não foi inicializado
			Initialize();
        _total = total;
        _current = current;
		percentageBar.value = (float)_current/(float)_total;
		OnPress(false);
    }

    void OnPress(bool isDown){
        percentageLabel.text = isDown ? (_current + " / " + _total) : (Mathf.RoundToInt(percentageBar.value * 100) + "%");
    }
}