﻿using UnityEngine;
using System.Collections;

public class AnchorUtil : MonoBehaviour {

    /// <summary>
    /// Calculate the position to anchor an object relative to it´s target (Like 'Set to Current Position' does on editor).
    /// É nescessário setar o target da ancora antes de chamar essa função.
    /// </summary>
    /// <param name="widget">The object to get anchored</param>
    /// <param name="target">The object's anchor's target</param>
    /// <param name="sideIndex">Which side of the object is being anchored. 0 = LEFT, 1 = TOP, 2 = RIGHT, 3 = BOTTOM.</param>
    /// <returns></returns>
    public static float CalculateRelativeAnchor(UIRect widget, int sideIndex)
    {
        Transform target = null;
        //left top right bottom
        bool[] IsHorizontal = { true, false, true, false };

        switch (sideIndex)
        {
            case 0:
                target = widget.leftAnchor.target;
                break;
            case 1:
                target = widget.topAnchor.target;
                break;
            case 2:
                target = widget.rightAnchor.target;
                break;
            case 3:
                target = widget.bottomAnchor.target;
                break;
        }

        if (target == null)
        {
            Debug.LogWarning("ERRO - O widget precisa ter um target pra poder calcular a posição da ancora");
            return 0;
        }

        UIRect myRect = widget;
        UIRect targetRect = target.GetComponent<UIRect>();

        Vector3[] sides = myRect.GetSides(targetRect.cachedTransform);

        float f = IsHorizontal[sideIndex] ? sides[sideIndex].x : sides[sideIndex].y;

        float targetWidth = targetRect.gameObject.GetComponent<UIWidget>().width;
        float targetHeight = targetRect.gameObject.GetComponent<UIWidget>().height;

        return IsHorizontal[sideIndex] ? f / targetWidth : 1 + (f / targetHeight);
    }
}
