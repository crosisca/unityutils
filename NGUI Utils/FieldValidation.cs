﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System;

public class FieldValidation : MonoBehaviour {

    UILabel label;

    public enum FieldValidationType {Name, Email, MatchingText, BirthDate, MininumCharacters, MatchingPassword }

    public FieldValidationType type;
    public UILabel matchLabel;
    public UILabel day, month, year;
    [SerializeField]
    int minimumCharacters;

    Color originalTextColor;
    Color originalSpriteColor;

    [SerializeField]
    UIInput matchPasswordInput;

    void Awake()
    {
        label = GetComponent<UILabel>();
        
        if(label != null)
            originalTextColor = label.color;

        if(GetComponentInParent<UISprite>() != null)
            originalSpriteColor = GetComponentInParent<UISprite>().color;
    }

    //Reset color on focus
    void Update()
    {
        if (GetComponentInParent<UIInput>() != null && GetComponentInParent<UIInput>().isSelected && label != null)
        {
            label.color = originalTextColor;
            if(GetComponentInParent<UISprite>() != null)
                GetComponentInParent<UISprite>().color = originalSpriteColor;
        }

        if (type == FieldValidationType.MatchingPassword && GetComponent<UIInput>() != null && GetComponent<UIInput>().isSelected)
        {
            if (GetComponent<UISprite>() != null)
                GetComponent<UISprite>().color = originalSpriteColor;
        }
        
    }
	
	public bool ValidateField()
    {
        switch (type)
	    {
		case FieldValidationType.Name:
            if (string.IsNullOrEmpty(label.text) ||
                string.IsNullOrEmpty(label.text.Replace(" ", string.Empty)))
            {
                Debug.Log("Error no label NAME: " + label.name);
                ErrorAt(label);
                return false;
            }
        break;
        case FieldValidationType.Email:
            if (!Regex.IsMatch(label.text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                Debug.Log("Error no label EMAIL: " + label.name);
                ErrorAt(label);
                return false;
            }
        break;
        case FieldValidationType.MatchingText:
            if (matchLabel != null)
            {
                if (label.text != matchLabel.text)
                {
                    Debug.Log("Error no label MATCHINGTEXT: " + label.name);
                    ErrorAt(label);
                    return false;
                }
            }
            else
            {
                Debug.LogError("Objeto com validacao do tipo MatchingText precisa ter o label pra comparar setado no inspector!");
            }
        break;
        case FieldValidationType.MatchingPassword:
        if (matchPasswordInput != null)
        {
            if (GetComponent<UIInput>().value != matchPasswordInput.value)
            {
                Debug.Log("Error no label MATCHINGPASSWORD: " + name);
                ErrorAt(GetComponent<UIInput>().label);
                return false;
            }
        }
        else
        {
            Debug.LogError("Objeto com validacao do tipo MatchingText precisa ter o label pra comparar setado no inspector!");
        }
        break;
        case FieldValidationType.BirthDate:
            if(day == null || month == null || year == null)
                Debug.LogError("Objeto com validacao do tipo BirthDate precisa ter os labels DIA, MES e ANO setados no inspector!");

            if(string.IsNullOrEmpty(day.text))
            {
                ErrorAt(day);
                return false;
            }
            if (string.IsNullOrEmpty(month.text))
            {
                ErrorAt(month);
                return false;
            }
            if (string.IsNullOrEmpty(year.text))
            {
                ErrorAt(year);
                return false;
            }
						
            if(int.Parse(day.text) > 31)
            {
                ErrorAt(day);
                return false;
            }
            if(int.Parse(month.text) > 12)
            {
                ErrorAt(month);
                return false;
            }
            
            if (int.Parse(year.text) < 1900)
            {
                ErrorAt(year);
                return false;
            }

            DateTime temp;
            string dateTime = month.text + "/" + day.text + "/" + year.text;
											
            if (!DateTime.TryParse(dateTime, out temp))
            {
                ErrorAt(day);
                ErrorAt(month);
                ErrorAt(year);
                return false;
            }
            
			if(temp.Year > DateTime.Today.Year)
			{
				ErrorAt(year);	
				return false;					
			}
			else if(temp.Year == DateTime.Today.Year)
			{
				if(temp.Month > DateTime.Today.Month)
				{
					ErrorAt(month);	
					return false;						
				}
				else if(temp.Month == DateTime.Today.Month)
				{
					if(temp.Day > DateTime.Today.Day)
					{
						ErrorAt(day);	
						return false;							
					}	
				}
			}			
        break;
        case FieldValidationType.MininumCharacters:
        if (label.text.Length < minimumCharacters)
        {
            Debug.Log("Error no label MINIMUM CHARACTERS: " + label.name);
            ErrorAt(label);
            return false;
        }
        break;
	    }   
        return true;
    }

    void ErrorAt(UILabel errorText)
    {
        
        errorText.color = new Color(0, 0, 0);
        
        UISprite labelBackground = errorText.GetComponentInParent<UISprite>();
        if (labelBackground != null)
            labelBackground.color = new Color(1, 0.7f, 0.7f);
        
    }
}
