﻿using UnityEngine;
using System.Collections;

public class ScreenBlocker
{

    /// <summary>
    /// Bloqueia a tela de qualquer input
    /// </summary>
    /// <returns>Objeto que está bloqueando a tela</returns>
    public static GameObject Block()
    {
        //Garante que só tem um bloqueador por vez
        if (GameObject.FindWithTag("ScreenBlocker") != null)
            GameObject.Destroy(GameObject.FindWithTag("ScreenBlocker"));

        GameObject root = GameObject.Find("UI Root");
        GameObject screenBlocker = NGUITools.AddChild(root, Resources.Load("ScreenBlocker") as GameObject);
        return screenBlocker;
    }

    /// <summary>
    /// Desbloqueia a tela e libera inputs
    /// </summary>
    public static void Unblock()
    {
        if(GameObject.FindWithTag("ScreenBlocker") != null)
            GameObject.Destroy(GameObject.FindWithTag("ScreenBlocker"));
    }
}
