using UnityEngine;

/// <summary>
/// Esse script é diferente do UIButtonScale. Ele mantém pressionado o botão conforme o usuário mantém pressionado, podendo afetar vários se o jogador continuar pressionando.
/// </summary>

[RequireComponent (typeof(Collider2D))]
public class ScaleTransformOnPress : MonoBehaviour
{
	public Transform tweenTarget;
	public Vector3 pressed = new Vector3(1.1f, 1.1f, 1.1f);
	public int mouseKey = 0;
	public float duration = 0.2f;

	Vector3 mScale;
	bool mStarted = false;
	bool hasPressed = false;

	void Start ()
	{
		if (!mStarted)
		{
			mStarted = true;
			if (tweenTarget == null) tweenTarget = transform;
			mScale = tweenTarget.localScale;
		}
	}

	void OnDisable ()
	{
		if (mStarted && tweenTarget != null)
		{
			TweenScale tc = tweenTarget.GetComponent<TweenScale>();

			if (tc != null)
			{
				tc.value = mScale;
				tc.enabled = false;
			}
		}
	}

	void Update(){
		bool isOver = UICamera.hoveredObject==gameObject;
		if(isOver && !hasPressed){
			TweenScale.Begin(tweenTarget.gameObject, duration, Vector3.Scale(mScale, pressed)).method = UITweener.Method.EaseInOut;
			hasPressed = true;
		}else if(hasPressed && !isOver){
			TweenScale.Begin(tweenTarget.gameObject, duration, mScale).method = UITweener.Method.EaseInOut;
			hasPressed = false;
		}
	}
}