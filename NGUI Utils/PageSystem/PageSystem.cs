﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PageSystem : MonoBehaviour {

    /// <summary>
    /// Number of columns to place to itens in
    /// </summary>
    [SerializeField]
    int columns;

    /// <summary>
    /// Sprite that fills the background of the page
    /// </summary>
    [SerializeField]
    UISprite backgroundSprite;

    /// <summary>
    /// List that stores all objects to be shown
    /// </summary>
//    public List<System.Object> prefabList = new List<System.Object>();

    /// <summary>
    /// Store the total ammount of itens to create the pages
    /// </summary>
    int prefabListSize = 0;

    /// <summary>
    /// Maximum number of itens to show on each page
    /// </summary>
    [SerializeField]
    int itensPerPage;

    /// <summary>
    /// If true ignores itensPerPage and use as many as the background can fit in
    /// </summary>
    [SerializeField]
    bool fitRelatedToBackgroundSize;

    /// <summary>
    /// If true adjusts background size to perfectly match itens per page height. It only decreases background size.
    /// </summary>
    [SerializeField]
    bool adjustBackgroundSize;

    /// <summary>
    /// Table where the itens should be place in
    /// </summary>
    [SerializeField]
    UITable table;

    /// <summary>
    /// Label that shows the current page
    /// </summary>
    [SerializeField]
    UILabel pageCounter;

    /// <summary>
    /// Stores the index of the first item been show on the page
    /// </summary>
    int currentStartIndex = 0;

    /// <summary>
    /// Prefab of the button to change pages, needs <UIButton>UIButton component</UIButton> and <UIToggle>UIToggle component</UIToggle>
    /// </summary>
    [SerializeField]
    GameObject pageButtonPrefab;

    /// <summary>
    /// Used to store all the page buttons created and delete them when needed
    /// </summary>
    List<GameObject> pageButtonList = new List<GameObject>();

    /// <summary>
    /// Container where the page buttons will be placed
    /// </summary>
    [SerializeField]
    UIWidget pageButtonsContainer;

    /// <summary>
    /// How bigger than the objects borders the page's background will be vertically
    /// </summary>
    [SerializeField]
    int verticalOffset = 0;

    /// <summary>
    /// How bigger than the objects borders the page's background will be horizontally
    /// </summary>
    [SerializeField]
    int horizontalOffset = 0;

    [SerializeField]
    UIButton buttonNext;

    [SerializeField]
    UIButton buttonPrev;

    /// <summary>
    /// Store the prefab that will be created to fill the page
    /// </summary>
    [HideInInspector]
    public GameObject prefabItem;

    public delegate void CallbackForSetup(GameObject obj, int index);
    public CallbackForSetup callbackForSetup;

    void Start()
    {
        buttonNext.gameObject.SetActive(false);
        buttonPrev.gameObject.SetActive(false);
        pageButtonsContainer.gameObject.SetActive(false);
    }

    void OnEnable()
    {
        if(callbackForSetup != null)
        Initialize(prefabListSize, callbackForSetup);
    }

    void OnDisable()
    {
        //Clear the page
        ClearTable();
    }

    public void Initialize(int itensListSize, CallbackForSetup callback)
	{
		prefabListSize = itensListSize;
		//Make sure there are itens to show
		if (prefabListSize == 0)
		{
			ClearTable();
			return;
		}
		currentStartIndex = 0;
        callbackForSetup = callback;

        StartCoroutine(WaitForResize());
    }

    IEnumerator WaitForResize()
    {
        yield return new WaitForEndOfFrame();
        //Calculate number of itens per page
        if (fitRelatedToBackgroundSize)
        {
            //Calculate prefab deform ratio related to anchor to preview it´s real size when instantiated
            int prefabWidth = prefabItem.GetComponent<UIWidget>().width;
            int prefabHeight = prefabItem.GetComponent<UIWidget>().height;
            float ratio1 = (float)backgroundSprite.width / (float)prefabWidth;
            float newPrefabHeight = prefabHeight * ratio1;
            itensPerPage = Mathf.FloorToInt(backgroundSprite.height / newPrefabHeight);
        }

        //Adjust background
        if (adjustBackgroundSize)
        {
            backgroundSprite.width = prefabItem.GetComponent<UIWidget>().width + horizontalOffset;
            backgroundSprite.height = itensPerPage * prefabItem.GetComponent<UIWidget>().height + verticalOffset;
            table.transform.localPosition = new Vector3(horizontalOffset / 2,
                                                        -verticalOffset / 2,
                                                        0);
        }

        //Set number of columns
        table.columns = columns;

        //Show itens
        UpdateShownItens();

        //Create buttons to change page
        CreatePageCounter();
    }

    /// <summary>
    /// Create the page buttons for fast switchting between them
    /// </summary>
    void CreatePageCounter()
    {
        //Remove all buttons if there are any
        foreach (GameObject pageButton in pageButtonList)
        {
            GameObject.Destroy(pageButton);
        }
        //Make sure the list doesn´t already have buttons
        pageButtonList.Clear();

        //Calculate number of pages
        int totalPages = Mathf.Max(1, Mathf.CeilToInt((float)prefabListSize / itensPerPage));

        if(totalPages > 1)
        {
            buttonNext.gameObject.SetActive(true);
            buttonPrev.gameObject.SetActive(true);
            pageButtonsContainer.gameObject.SetActive(true);

            //Create a button for each page
            for (int i = 0; i < totalPages; i++)
            {
                GameObject pgButton = NGUITools.AddChild(pageButtonsContainer.gameObject, pageButtonPrefab);
                pgButton.name = "PageButton" + i;
                pgButton.GetComponent<PageSystemPageButton>().pageNumber = i;
                pgButton.GetComponent<PageSystemPageButton>().OnClicked += OnPageButtonClicked;
                UISprite pgButtonSprite = pgButton.GetComponent<UISprite>();

                // Position
                pgButton.transform.localScale = Vector3.one;
                pgButton.transform.localPosition = new Vector3(((1f / (totalPages + 1f)) * pageButtonsContainer.width) * (i + 1),
                                                                -pageButtonsContainer.height / 2,
                                                                0);

                //Scale button according to quantity of them
                if (pgButtonSprite.width * totalPages > pageButtonsContainer.width)
                    pgButtonSprite.width = pageButtonsContainer.width / (totalPages + (totalPages / 2));

                // Anchor
                pgButtonSprite.leftAnchor.target = pageButtonsContainer.transform;
                pgButtonSprite.leftAnchor.relative = CalculateRelativeAnchor(pgButtonSprite, 0);
                pgButtonSprite.rightAnchor.target = pageButtonsContainer.transform;
                pgButtonSprite.rightAnchor.relative = CalculateRelativeAnchor(pgButtonSprite, 2);
                pgButtonSprite.ResetAnchors();
                pgButtonSprite.UpdateAnchors();

                //Add the buttons to a list
                pageButtonList.Add(pgButton);
            }

            //Activate the first page
            pageButtonList[0].GetComponent<UIToggle>().value = true;
        }
        else
        {
            buttonNext.gameObject.SetActive(false);
            buttonPrev.gameObject.SetActive(false);
            pageButtonsContainer.gameObject.SetActive(false);
        }
        
    }

    /// <summary>
    /// Calculate the position to anchor an object relative to it´s target (Like 'Set to Current Position' does on editor).
    /// É nescessário setar o target da ancora antes de chamar essa função.
    /// </summary>
    /// <param name="widget">The object to get anchored</param>
    /// <param name="target">The object's anchor's target</param>
    /// <param name="sideIndex">Which side of the object is being anchored. 0 = LEFT, 1 = TOP, 2 = RIGHT, 3 = BOTTOM.</param>
    /// <returns></returns>
    float CalculateRelativeAnchor(UIRect widget, int sideIndex)
    {
        Transform target = null;
        //left top right bottom
        bool[] IsHorizontal = {true,false,true,false};

        switch (sideIndex)
	    {
            case 0:
                target = widget.leftAnchor.target;
                break;
            case 1:
                target = widget.topAnchor.target;
                break;
            case 2:
                target = widget.rightAnchor.target;
                break;
            case 3:
                target = widget.bottomAnchor.target;
                break;
	    }

        if(target == null)
        {
            Debug.LogWarning("ERRO - O widget precisa ter um target pra poder calcular a posição da ancora");
            return 0;
        }

        UIRect myRect = widget;
        UIRect targetRect = target.GetComponent<UIRect>();

        Vector3[] sides = myRect.GetSides(targetRect.cachedTransform);

        float f = IsHorizontal[sideIndex] ? sides[sideIndex].x : sides[sideIndex].y;

        float targetWidth = targetRect.gameObject.GetComponent<UIWidget>().width;
        float targetHeight = targetRect.gameObject.GetComponent<UIWidget>().height;

        return IsHorizontal[sideIndex] ? f / targetWidth : 1 + (f / targetHeight);
    }

    void OnPageButtonClicked(int pageNumber)
    {
        //Stores index of first item to show on the page
        currentStartIndex = pageNumber * itensPerPage;

        //Update page
        UpdateShownItens();
    }

    /// <summary>
    /// Update itens shown in the page, in relation to 'currentStartIndex'
    /// </summary>
    public void UpdateShownItens()
    {
        //Make sure there are itens to show (is it necessary?)
//        if (prefabList.Count == 0)
        if (prefabListSize == 0)
            return;

        //Clear the page
        ClearTable();

        //Add itens to the table
        for (int i = 0; i < itensPerPage; i++)
        {
//            if(currentStartIndex + i < prefabList.Count)
            if (currentStartIndex + i < prefabListSize)
            {
                GameObject go = NGUITools.AddChild(table.gameObject, prefabItem);
                callbackForSetup(go, currentStartIndex + i);
            }
        }

        //Fix itens positions
        table.Reposition();

        foreach (Transform item in table.GetChildList())
        {
            UIWidget itemWidget = item.GetComponent<UIWidget>();
            //itemWidget.keepAspectRatio = UIWidget.AspectRatioSource.BasedOnHeight;
            //itemWidget.topAnchor.target = backgroundSprite.transform;
            //itemWidget.topAnchor.relative = CalculateRelativeAnchor(itemWidget, 1);
            //itemWidget.bottomAnchor.target = backgroundSprite.transform;
            //itemWidget.bottomAnchor.relative = CalculateRelativeAnchor(itemWidget, 3);
            //itemWidget.ResetAndUpdateAnchors();

            //TODO -testar se deve ancorar no width e dos lados ou no height e emcima e embaixo
            //aparentemente funciona baseado em width \/
            itemWidget.keepAspectRatio = UIWidget.AspectRatioSource.BasedOnWidth;
            itemWidget.leftAnchor.target = backgroundSprite.transform;
            itemWidget.leftAnchor.relative = 0;
            itemWidget.rightAnchor.target = backgroundSprite.transform;
            itemWidget.rightAnchor.relative = 1;
            itemWidget.ResetAndUpdateAnchors();
        }

        Invoke("RepositionTable", 0.1f);

        //Update page indicator
        UpdatePageCounter();
    }

    void RepositionTable()
    {
        table.Reposition();
    }

    /// <summary>
    /// Clear all objects in the page
    /// </summary>
    public void ClearTable()
    {
        //Destroys all objects in the page
        foreach (Transform item in table.GetChildList())
        {
            item.parent = null;
            Destroy(item.gameObject);
        }

        //Reset positions
        table.Reposition();
    }

    /// <summary>
    /// Called when clicked on previous page
    /// </summary>
    public void OnClickNext()
    {
        
        //Make sure it´s not already in the last page
//        if (currentStartIndex + itensPerPage >= prefabList.Count)
        if (currentStartIndex + itensPerPage >= prefabListSize)
            return;

        //Stores index of first item to show on the page
        currentStartIndex += itensPerPage;

        //Calculate current page (zero based index)
        pageButtonList[currentStartIndex / itensPerPage].GetComponent<UIToggle>().value = true;

        //Update page
        UpdateShownItens();
    }

    /// <summary>
    /// Called when clicked on next page
    /// </summary>
    public void OnClickPrev()
    {
        //Make sure it´s not already in the first page
        if (currentStartIndex - itensPerPage < 0)
            return;

        //Stores index of first item to show on the page
        currentStartIndex -= itensPerPage;

        //Calculate current page (zero based index)
        pageButtonList[currentStartIndex / itensPerPage].GetComponent<UIToggle>().value = true;

        //Update page
        UpdateShownItens();
    }

    /// <summary>
    /// Calculate current page been show and total number of pages and show it on a label.
    /// </summary>
    void UpdatePageCounter()
    {
        if (pageCounter == null)
            return;

        int currentPage = 1 + currentStartIndex/itensPerPage;
//        int totalPages = Mathf.Max(1, Mathf.CeilToInt((float)prefabList.Count / itensPerPage));
        int totalPages = Mathf.Max(1, Mathf.CeilToInt((float)prefabListSize / itensPerPage));
        pageCounter.text = currentPage.ToString() + "/" + totalPages.ToString();
    }



    //debug only
    //public GameObject prefab;
    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.A))
    //    {
    //        prefabList.Add(prefab);
    //    }
    //    if (Input.GetKeyDown(KeyCode.R))
    //    {
    //        prefabList.RemoveAt(prefabList.Count - 1);
    //    }
    //    if (Input.GetKeyDown(KeyCode.U))
    //    {
    //        UpdateShownItens();
    //    }

    //    UpdatePageCounter();
    //}
}
