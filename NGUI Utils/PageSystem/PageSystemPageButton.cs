﻿using UnityEngine;
using System.Collections;

public class PageSystemPageButton : MonoBehaviour {

    public int pageNumber = 0;

    public delegate void PageButtonEvent(int pgNum);
    public event PageButtonEvent OnClicked;

    void OnClick()
    {
        OnClicked(pageNumber);
    }
}
