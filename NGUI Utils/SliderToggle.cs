﻿using UnityEngine;
using System.Collections;

public class SliderToggle : MonoBehaviour {

    UISlider slider;

    void Awake()
    {
        //slider = GetComponentInParent<UISlider>();
    }

	void OnClick()
    {
        slider.value = Mathf.Abs(slider.value - 1);
    }

    public void OnChange()
    {
        slider = GetComponentInParent<UISlider>();
        if (slider.value == 1)
            GetComponent<UISprite>().spriteName = "Toggle_On";
        else
            GetComponent<UISprite>().spriteName = "Toggle_Off";
    }
}
