﻿using UnityEngine;
using System.Collections;

public class SetFocusToNextField : MonoBehaviour {

    public UIInput fieldToFocus;

    public void SetFocus(){
        StartCoroutine(DelayToFocus());
    }

    IEnumerator DelayToFocus()
    {
        yield return null;
        if (fieldToFocus != null)
            fieldToFocus.isSelected = true;
    }
}
