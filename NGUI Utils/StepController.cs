﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using StepHelper;
	
public class StepController : MonoBehaviour {

	public enum TypeOfStep {
		ScrollUp,
		ScrollDown,
		ScrollRight,
		ScrollLeft,
		FadeOutAndIn,
		FadeOut,
		CleanCut
	};

	public TypeOfStep typeOfForwardStep;
	public TypeOfStep typeOfBackStep;

	public List<GameObject> steps = new List<GameObject> ();

	public float animSpeed = 0f;
	public int stepStartingSortOrder = 0;

	List<int> lastSteps = new List<int> ();
	int currentStep = 0;
	int nextStep = 0;

	int cardHeight = 0;
	int cardWidth = 0;

	bool finishedNext = false;
	bool finishedLast = false;

	public void Start () {
		cardHeight = Mathf.CeilToInt(Screen.width * GetComponentInParent<UIRoot>().pixelSizeAdjustment);
		cardWidth = Mathf.CeilToInt(Screen.width * GetComponentInParent<UIRoot>().pixelSizeAdjustment);

		foreach (GameObject s in steps) {
			UIPanel objectPanel = null;
			if (s.GetComponent<UIPanel>() == null) {
				Debug.LogError ("Steps need to be UIPanel objects. Please check <" + s.name + "> GameObject.");
				continue;
			} else {
				objectPanel = s.GetComponent<UIPanel> ();
			}

			if (s == steps.First ()){
				objectPanel.GetComponent<UIPanel> ().alpha = 1;
			}
			else {
				objectPanel.GetComponent<UIPanel> ().alpha = 0;
			}
			objectPanel.sortingOrder = stepStartingSortOrder;
			
			UIRect rect = s.GetComponent<UIRect>();
			if(rect != null)
			{
				rect.leftAnchor = null;
				rect.topAnchor = null;
				rect.rightAnchor = null;
				rect.bottomAnchor = null;
			}
		}
	}

	public void Update () {
		if (finishedNext) {
			steps [currentStep].GetComponent<UIWidget>().alpha = 0;
			lastSteps.Push (currentStep);
			currentStep++;
			finishedNext = false;
		}

		if (finishedLast) {
			steps [currentStep].GetComponent<UIWidget>().alpha = 0;
			currentStep = nextStep;
			finishedLast = false;
		}
	}

	public void NextStep () {
		if (currentStep >= steps.Count-1) {
			return;
		}

		nextStep = currentStep + 1;

		StartAnimToStep (true, nextStep);
	}

	public void LastStep () {
		if (!lastSteps.Any ()) {
			return;
		}

		nextStep = lastSteps.Pop ();

		StartAnimToStep (false, nextStep);
	}

	public void GoToStep (int step) {
		if (step >= steps.Count-1) {
			return;
		}

		nextStep = step;

		StartAnimToStep (false, nextStep);
	}

	void StartAnimToStep (bool isForward, int nextStep = -1) {

		TypeOfStep typeOfAnim = (isForward ? typeOfForwardStep : typeOfBackStep);
		int animDirection = (isForward ? 1 : 2);

		IEnumerator lastStep = null;
		IEnumerator firstStep = null;

		switch (typeOfAnim) {

		case TypeOfStep.ScrollUp:
			SetStepForAnim (typeOfAnim, steps [nextStep]);
			lastStep = ScrollVertical (nextStep, true, animDirection);
			firstStep = ScrollVertical (currentStep, true, 0,lastStep);
			break;

		case TypeOfStep.ScrollDown:	
			SetStepForAnim (typeOfAnim, steps [nextStep]);
			lastStep = ScrollVertical (nextStep, false, animDirection);
			firstStep = ScrollVertical (currentStep, false, 0, lastStep);
			break;

		case TypeOfStep.ScrollRight:	
			SetStepForAnim (typeOfAnim, steps [nextStep]);
			lastStep = ScrollHorizontal (nextStep, true, animDirection);
			firstStep = ScrollHorizontal (currentStep, true, 0, lastStep);
			break;

		case TypeOfStep.ScrollLeft:	
			SetStepForAnim (typeOfAnim, steps [nextStep]);
			lastStep = ScrollHorizontal (nextStep, false, animDirection);
			firstStep = ScrollHorizontal (currentStep, false, 0, lastStep);
			break;

		case TypeOfStep.FadeOutAndIn:	
			SetStepForAnim (typeOfAnim, steps [nextStep]);
			lastStep = FadeOutAndIn (nextStep, true, animDirection);
			firstStep = FadeOutAndIn (currentStep, false, 0, lastStep);
			break;

		case TypeOfStep.FadeOut:
			SetStepForAnim (typeOfAnim, steps [nextStep]);
			firstStep = FadeOut (animDirection);
			break;

		case TypeOfStep.CleanCut:
			SetStepForAnim (typeOfAnim, steps [nextStep]);
			firstStep = CleanCut (nextStep, animDirection);
			break;
		}

		StartCoroutine (firstStep);
	}

	IEnumerator ScrollVertical (int step, bool directionUp, int animHierarchy, IEnumerator nextAnim = null) {
		if (nextAnim != null) {
			StartCoroutine (nextAnim);
		}

		Vector3 stepPos = steps[step].transform.localPosition;
		float initialY = steps[step].transform.localPosition.y;

		float t = 0f;

		while (t < animSpeed) {
			float newPosY = Mathf.Lerp(0, (directionUp ? cardHeight : -cardHeight), Mathf.SmoothStep(0.0f, 1.0f, t/animSpeed));

			steps[step].transform.localPosition = new Vector3 (stepPos.x, initialY+newPosY, stepPos.z);

			t += Time.deltaTime;
			yield return null;
		}

		steps[step].transform.localPosition = new Vector3 (0, initialY + (directionUp ? cardHeight : -cardHeight), 0);

		finishedNext |= animHierarchy == 1;
		finishedLast |= animHierarchy == 2;
	}

	IEnumerator ScrollHorizontal (int step, bool directionRight, int animHierarchy, IEnumerator nextAnim = null) {
		if (nextAnim != null) {
			StartCoroutine (nextAnim);
		}

		Vector3 stepPos = steps[step].transform.localPosition;
		float initialX = steps[step].transform.localPosition.x;

		float t = 0f;

		while (t < animSpeed) {
			float newPosX = Mathf.Lerp(0, (directionRight ? cardWidth : -cardWidth), Mathf.SmoothStep(0.0f, 1.0f, t/animSpeed));


			steps[step].transform.localPosition = new Vector3 (initialX+newPosX, stepPos.y, stepPos.z);

			t += Time.deltaTime;
			yield return null;
		}

		steps[step].transform.localPosition = new Vector3 (initialX + (directionRight ? cardWidth : -cardWidth), 0, 0);

		finishedNext |= animHierarchy == 1;
		finishedLast |= animHierarchy == 2;
	}

	IEnumerator FadeOutAndIn (int step, bool directionIn, int animHierarchy, IEnumerator nextAnim = null) {
		float t = 0f;

		while (t < animSpeed) {
			float newAlpha = Mathf.Lerp((directionIn ? 0 : 1), (directionIn ? 1 : 0), Mathf.SmoothStep(0.0f, 1.0f, t/animSpeed));
			steps [step].GetComponent<UIWidget> ().alpha = newAlpha;

			t += Time.deltaTime;
			yield return null;
		}

		steps [step].GetComponent<UIWidget> ().alpha = (directionIn ? 1 : 0);

		if (nextAnim != null) {
			StartCoroutine (nextAnim);
		}

		finishedNext |= animHierarchy == 1;
		finishedLast |= animHierarchy == 2;
	}

	IEnumerator FadeOut (int animHierarchy) {
		float t = 0f;

		while (t < animSpeed) {
			float newAlpha = Mathf.Lerp(1, 0, Mathf.SmoothStep(0.0f, 1.0f, t/animSpeed));
			steps [currentStep].GetComponent<UIWidget> ().alpha = newAlpha;

			t += Time.deltaTime;
			yield return null;
		}

		steps [currentStep].GetComponent<UIWidget> ().alpha = 0;

		finishedNext |= animHierarchy == 1;
		finishedLast |= animHierarchy == 2;
	}

	IEnumerator CleanCut (int step, int animHierarchy) {
		yield return null;

		steps [currentStep].GetComponent<UIWidget> ().alpha = 0;

		steps [step].GetComponent<UIWidget> ().alpha = 1;

		finishedNext |= animHierarchy == 1;
		finishedLast |= animHierarchy == 2;
	}

	void SetStepForAnim (TypeOfStep type, GameObject forwardStep) {
		steps[currentStep].GetComponent<UIWidget> ().depth++;
		forwardStep.GetComponent<UIWidget> ().depth--;

		switch (type) {
		case TypeOfStep.ScrollUp:
			forwardStep.GetComponent<UIWidget> ().alpha = 1;
			forwardStep.transform.localPosition = new Vector3 (0, -cardHeight, 0);
			break;

		case TypeOfStep.ScrollDown:
			forwardStep.GetComponent<UIWidget> ().alpha = 1;
			forwardStep.transform.localPosition = new Vector3 (0, cardHeight, 0);
			break;

		case TypeOfStep.ScrollRight:
			forwardStep.GetComponent<UIWidget> ().alpha = 1;
			forwardStep.transform.localPosition = new Vector3 (-cardWidth, 0, 0);
			break;

		case TypeOfStep.ScrollLeft:
			forwardStep.GetComponent<UIWidget> ().alpha = 1;
			forwardStep.transform.localPosition = new Vector3 (cardWidth, 0, 0);
			break;

		case TypeOfStep.FadeOutAndIn:
			forwardStep.GetComponent<UIWidget> ().alpha = 0;
			forwardStep.transform.localPosition = new Vector3 (0, 0, 0);
			break;

		case TypeOfStep.FadeOut:
			forwardStep.GetComponent<UIWidget> ().alpha = 1;
			forwardStep.transform.localPosition = new Vector3 (0, 0, 0);
			break;

		case TypeOfStep.CleanCut:
			forwardStep.GetComponent<UIWidget> ().alpha = 0;
			forwardStep.transform.localPosition = new Vector3 (0, 0, 0);
			break;
		}
	}
}

namespace StepHelper {
	public static class StepHelper {
		public static T Pop<T> (this List<T> list) {
			T obj = list [list.Count-1];
			list.RemoveAt(list.Count-1);
			return obj;
		}

		public static List<T> Push<T> (this List<T> list, T obj) {
			if (list.Count >= 10)
				list.RemoveAt (list.Count-1);
			list.Add (obj);
			return list;
		}
	}
}