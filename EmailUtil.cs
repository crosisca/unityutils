﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using ConnectionTools;

public class EmailUtil {
    public const string EMAIL_TO = "gatopolismail@gmail.com";
    const string EMAIL_FROM = "gatopolismail@gmail.com";
    const string EMAIL_PASSWORD = "gatopolis2015";
    const string SMTP_SERVER = "smtp.gmail.com";
    const int PORT = 587;

    /// <summary>
    /// Verifica se um endereço de email é válido
    /// </summary>
    /// <returns></returns>
    public static bool ValidadeEmail(string email)
    {
        if (string.IsNullOrEmpty(email))
            return false;

        if (Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            return true;
        return false;
    }


    /// <summary>
    /// Exibe a tela de enviando email, e agenda a chamada pra realmente enviar
    /// </summary>
    /// <param name="title"></param>
    /// <param name="body"></param>
    /// <param name="to"></param>
    public static void SendEmail(string title, string body, string to = EMAIL_TO, System.Action<bool> callback = null)
    {
        GameObject sendingMailPopup = PopupHandler.ShowLoadingMessage("Aguarde, enviando email");
        
        sendingMailPopup.GetComponent<PopupLoadingMessage>().StartCoroutine(DelaySendEmail(title, body, to, callback));
    }

    /// <summary>
    /// Aguarda um tempo enquanto o popup de enviando email é exibido, Verifica se tem internet e se o email receptor é valido e tenta enviar um email
    /// </summary>
    /// <param name="title"></param>
    /// <param name="body"></param>
    /// <param name="to"></param>
    /// <returns></returns>
    static IEnumerator DelaySendEmail(string title, string body, string to, System.Action<bool> callback = null)
    {
        yield return new WaitForSeconds(0.3f);

		if (HttpConnectionTest.CurrentStatus == ConnectionStatus.Connected)
        {
            if(string.IsNullOrEmpty(to))
            {
				OnFailed(new System.Exception("Email de destinatário inválido"));
                yield break;
            }

            if (ValidadeEmail(to))
                ActuallySendEmail(title, body, to, callback);
            else
                OnFailed(new System.Exception("Email de destinatário inválido"));
        }
        else
        {
            OnFailed(new System.Exception("Você não está conectado a internet"));
        }
    }


    /// <summary>
    /// Envia realmente o email
    /// </summary>
    /// <param name="title">Assunto</param>
    /// <param name="body">Mensagem</param>
    /// <param name="to">Receptor</param>
    static void ActuallySendEmail(string title, string body, string to, System.Action<bool> callback = null)
    {
        
        try
        {
            MailMessage mail = new MailMessage();

            mail.IsBodyHtml = true;

            mail.From = new MailAddress(EMAIL_FROM);
            mail.To.Add(to);
            mail.Subject = title;
            mail.Body = body;

            SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
            smtpServer.Port = 587;
            smtpServer.Credentials = new System.Net.NetworkCredential(EMAIL_FROM, EMAIL_PASSWORD) as ICredentialsByHost;
            smtpServer.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
                delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };
            smtpServer.Send(mail);

            if (callback != null)
                callback(true);
            else
                OnSent(to);
        }
        catch (System.Exception e)
        {
            OnFailed(e);
            throw;
        }
    }

    //Chamado caso o email seja enviado com sucesso
    static void OnSent(string to)
    {
        PopupHandler.ShowWarning("Email", "Email enviado com sucesso", "OK");
        Debug.Log("Email enviado com sucesso para: " + to);
    }

    //Chamado caso o email falhe ao enviar
    static void OnFailed(System.Exception e)
    {
        PopupHandler.ShowWarning("Erro", "Ocorreu um erro ao enviar o email, verifique sua conexão e tente novamente", "OK");
        Debug.Log("Error trying to send email - " + e.Message);
    }
}