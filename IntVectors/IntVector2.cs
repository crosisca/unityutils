﻿using UnityEngine;
using System;

/// <summary>
/// <para>Basicamente um Vector2 que usa float ao inves de int.</para>
/// <para>Criei pq tava de saco cheio de ficar convertendo float pra int na hora de usar vector2. Alias, ele e mais rapido que o vector2, por usar ints.</para>
/// </summary>
[Serializable]
public struct IntVector2
{
	public int x;
	public int y;

	public IntVector2 (int x, int y)
	{	
		this.x = x;
		this.y = y;				
	}

	public IntVector2 (Vector2 vector2)
	{
		x = Mathf.RoundToInt(vector2.x);
		y = Mathf.RoundToInt(vector2.y);
	}

	public IntVector2 absolute
	{
		get
		{
			return new IntVector2(Mathf.Abs(this.x), Mathf.Abs(this.y));		
		}
	}

	public static IntVector2 zero
	{
		get
		{
			return new IntVector2(0,0);
		}		
	}	

	public IntVector2 clamped01
	{
		get
		{
			return IntVector2.Clamp01(new IntVector2(this.x,this.y));
		}
	}

	///Clampa entre -1 e 1
	public IntVector2 clampedMagnitudeOf1
	{
		get
		{
			return IntVector2.Clamp(new IntVector2(this.x,this.y), -1, 1);
		}
	}

	public override string ToString()
	{
		return String.Format("({0:F1}, {1:F1})", new object[]
			{
				this.x,
				this.y
			});
	}

	public override bool Equals(object other)
	{
		if (!(other is IntVector2))
		{
			return false;
		}
		IntVector2 vector = (IntVector2)other;
		return this.x.Equals(vector.x) && this.y.Equals(vector.y);
	}


	public static IntVector2 Clamp01 (IntVector2 vector)
	{
		if (vector.x < 0)
		{
			vector.x = 0;
		}
		else
		{
			if (vector.x > 1)
			{
				vector.x = 1;
			}
		}

		if (vector.y < 0)
		{
			vector.y = 0;
		}
		else
		{
			if (vector.y > 1)
			{
				vector.y = 1;
			}
		}		
		return vector;		
	}	

	public static IntVector2 Clamp (IntVector2 vector, int min, int max)
	{
		if (vector.x < min)
		{
			vector.x = min;
		}
		else
		{
			if (vector.x > max)
			{
				vector.x = max;
			}
		}

		if (vector.y < min)
		{
			vector.y = min;
		}
		else
		{
			if (vector.y > max)
			{
				vector.y = max;
			}
		}	
		return vector;
	}

	public static int SqrMagnitude(IntVector2 a)
	{
		return a.x * a.x + a.y * a.y;
	}

	public override int GetHashCode()
	{
		return this.x.GetHashCode() ^ this.y.GetHashCode() << 2;
	}

	public static IntVector2 operator -(IntVector2 a)
	{
		return new IntVector2(-a.x, -a.y);
	}

	public static IntVector2 operator -(IntVector2 a, IntVector2 b)
	{
		return new IntVector2(a.x - b.x, a.y - b.y);
	}

	public static IntVector2 operator +(IntVector2 a, IntVector2 b)
	{
		return new IntVector2(a.x + b.x, a.y + b.y);
	}

	public static bool operator ==(IntVector2 lhs, IntVector2 rhs)
	{
		return (lhs.x == rhs.x && lhs.y == rhs.y);
	}

	public static bool operator !=(IntVector2 lhs, IntVector2 rhs)
	{
		return !(lhs.x == rhs.x && lhs.y == rhs.y);
	}	

	//converte Point pra vector2
	public static implicit operator Vector2(IntVector2 p)
	{
		return new Vector2(p.x, p.y);
	}

	//converte vector 2 pra point
	public static implicit operator IntVector2(Vector2 v)
	{
		return new IntVector2(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y));
	}

	//converte point pra vector3
	public static implicit operator Vector3(IntVector2 p)
	{
		return new Vector3(p.x, p.y, 0f);
	}

	//converte vector3 pra point
	public static implicit operator IntVector2(Vector3 v)
	{
		return new IntVector2(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y));
	}
}