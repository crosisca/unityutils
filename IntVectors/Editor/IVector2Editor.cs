﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(IntVector2))]
public class IntVector2Editor : PropertyDrawer 
{
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		label = EditorGUI.BeginProperty(position,label,property);
		Rect contentPosition = EditorGUI.PrefixLabel(position, label);
		EditorGUI.PrefixLabel(position,label);
		contentPosition.width *= 0.325f;
		EditorGUIUtility.labelWidth = 14f;
		EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("x"), new GUIContent("X"));
		contentPosition.x += contentPosition.width + 0.15f;
		EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("y"), new GUIContent("Y"));
		EditorGUI.EndProperty();
	}
}
