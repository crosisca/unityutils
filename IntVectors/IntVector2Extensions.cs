﻿public static class IntVectorExtensions 
{
	/// <summary>
	/// Retorna o elemento do indice [x,y] do array utilizando um IntVector.
	/// </summary>
	public static T Get<T>(this T[,] array, IntVector2 indexes)
	{
		return array [indexes.x, indexes.y];
	}

	/// <summary>
	/// Insere o elemento do indice [x,y] do array utilizando um IntVector.
	/// </summary>
	public static T Set<T>(this T[,] array, IntVector2 indexes, T value)
	{
		array [indexes.x, indexes.y] = value;
		return value;
	}
}
