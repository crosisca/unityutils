﻿using UnityEngine;
using System;

[Serializable]
public struct IVector3
{
	public int x;
	public int y;
	public int z;

	public IVector3 (int x, int y, int z)
	{	
		this.x = x;
		this.y = y;	
		this.z = z;
	}

	public IVector3 (Vector3 vector3)
	{
		x = Mathf.RoundToInt(vector3.x);
		y = Mathf.RoundToInt(vector3.y);
		z = Mathf.RoundToInt(vector3.z);
	}

	public static IVector3 forward
	{
		get{return new IVector3 (0, 0, 1);}
	}

	public static IVector3 back
	{
		get{return new IVector3 (0, 0, -1);}
	}

	public static IVector3 right
	{
		get{return new IVector3 (1, 0, 0);}
	}

	public static IVector3 left
	{
		get{return new IVector3 (-1, 0, 0);}
	}

	public static IVector3 up
	{
		get{return new IVector3 (0, 1, 0);}
	}

	public static IVector3 down
	{
		get{return new IVector3 (0, -1, 0);}
	}

	public static IVector3 one
	{
		get{return new IVector3 (1, 1, 1);}
	}

	public static IVector3 zero
	{
		get{return new IVector3 (0, 0, 0);}
	}

	public float magnitude
	{
		get{ return Mathf.Sqrt (x * x + y * y + z * z); }
	}

	public int SqrMagnitude
	{
		get{return x * x + y * y + z * z;}
	}

	public override int GetHashCode()
	{
		return x.GetHashCode() ^ y.GetHashCode() << 2 ^ z.GetHashCode() >> 2;
	}

	public override string ToString()
	{
		return String.Format("({0:F1}, {1:F1}, {2:F1})", x,y,z);
	}

	public override bool Equals(object other)
	{
		if (!(other is IVector3))
		{
			return false;
		}
		IVector3 vector = (IVector3)other;
		return x.Equals(vector.x) && y.Equals(vector.y) && z.Equals(vector.z);
	}

	public static IVector3 operator -(IVector3 a)
	{
		return new IVector3(-a.x, -a.y, -a.z);
	}

	public static IVector3 operator -(IVector3 a, IVector3 b)
	{
		return new IVector3(a.x - b.x, a.y - b.y, a.z - b.z);
	}

	public static IVector3 operator +(IVector3 a, IVector3 b)
	{
		return new IVector3(a.x + b.x, a.y + b.y, a.z + b.z);
	}

	public static bool operator ==(IVector3 lhs, IVector3 rhs)
	{
		return (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z);
	}

	public static bool operator !=(IVector3 lhs, IVector3 rhs)
	{
		return !(lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z);
	}	

	//iv3 > iv2
	public static implicit operator IntVector2(IVector3 p)
	{
		return new IntVector2(p.x, p.y);
	}

	//iv2 > iv3
	public static implicit operator IVector3(IntVector2 p)
	{
		return new IVector3(p.x, p.y, 0);
	}

	//iv3 > v2
	public static implicit operator Vector2(IVector3 p)
	{
		return new Vector2(p.x, p.y);
	}

	//iv3 > v3
	public static implicit operator Vector3(IVector3 p)
	{
		return new Vector3(p.x, p.y, p.z);
	}

	//v2 > iv3
	public static implicit operator IVector3(Vector2 v)
	{
		return new IVector3(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y), 0);
	}

	//v3 > iv3
	public static implicit operator IVector3(Vector3 v)
	{
		return new IVector3(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y), Mathf.RoundToInt (v.z));
	}
}