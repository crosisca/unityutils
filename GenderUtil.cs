﻿using UnityEngine;
using System.Collections;

public class GenderUtil
{

    public const string MENINO = "Menino";//masculino
    public const string MENINA = "Menina";//feminino

    public static string CharToString(char c)
    {
        string gender = "";
        switch (c)
        {
            case 'M':
                gender = MENINO;
                break;
            case 'F':
                gender = MENINA;
                break;
        }
        return gender;
    }

    public static char StringToChar(string s)
    {
        char c = '\0';
        switch (s)
        {
            case MENINO:
                c = 'M';
                break;
            case MENINA:
                c = 'F';
                break;
        }
        return c;
    }

}
