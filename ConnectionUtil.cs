﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Net;

public class ConnectionUtil
{
	//TODO verificar a conexão com o nosso servidor, pra avisar pro usuario quando o problema da conexao é nosso e não dele.
    public static bool IsConnected()
    {
        WebClient client = null;
        Stream stream = null;
        try
        {
            client = new System.Net.WebClient();
            stream = client.OpenRead("http://www.google.com");
            return true;
        }
        catch (System.Exception e)
        {
            Debug.Log(e);
            return false;
        }
        finally
        {
            if (client != null)
                client.Dispose();
            if (stream != null)
                stream.Dispose();
        }
    }

	public static bool GetConnectionTesterStatus () {
		ConnectionTesterStatus cts = Network.TestConnection ();

		switch (cts){
		case ConnectionTesterStatus.Error:
			Debug.Log ("Connection Error!");
			return false;
		case ConnectionTesterStatus.Undetermined:
			Debug.Log ("Connection Undetermined!");
			return false;
		case ConnectionTesterStatus.PublicIPIsConnectable:
			Debug.Log ("Connection PublicIPIsConnectable!");
			return true;
		case ConnectionTesterStatus.PublicIPPortBlocked:
			Debug.Log ("Connection PublicIPPortBlocked!");
			return false;
		case ConnectionTesterStatus.PublicIPNoServerStarted:
			Debug.Log ("Connection PublicIPNoServerStarted!");
			return false;
		case ConnectionTesterStatus.LimitedNATPunchthroughPortRestricted:
			Debug.Log ("Connection LimitedNATPunchthroughPortRestricted!");
			return true;
		case ConnectionTesterStatus.LimitedNATPunchthroughSymmetric:
			Debug.Log ("Connection LimitedNATPunchthroughSymmetric!");
			return true;
		case ConnectionTesterStatus.NATpunchthroughAddressRestrictedCone:
			Debug.Log ("Connection NATpunchthroughAddressRestrictedCone!");
			return true;
		case ConnectionTesterStatus.NATpunchthroughFullCone:
			Debug.Log ("Connection NATpunchthroughFullCone!");
			return true;
		default:
			Debug.Log ("Connection default!");
			return false;
		}
	}
}