﻿using UnityEngine;
using System.Collections;
using System;

public class DateUtil {
	
	public static readonly long NANO = 1L;
	public static readonly long MILIS_IN_NANOS = 10000L * NANO;
	public static readonly long SECS_IN_NANOS = 1000L * MILIS_IN_NANOS;
	public static readonly long MIN_IN_NANOS = 60L * SECS_IN_NANOS;
	public static readonly long HOUR_IN_NANOS = 60L * MIN_IN_NANOS;
	public static readonly long DAY_IN_NANOS = 24L * HOUR_IN_NANOS;
	public static readonly long JAVA_01_01_1970_IN_NANOS = 62135600000000L * MILIS_IN_NANOS;

	public static DateTime GetDateByLong (long time) {
		/*long cs = 0L;
		if (time < JAVA_01_01_1970_IN_NANOS) {
			while (time < 10000000000000000L)
				time *= 10L;
			
			cs = JAVA_01_01_1970_IN_NANOS + time;
		}else{
			cs = time;
		}*/

		return new DateTime (time);
	}

	public static string ConvertDateToSql (System.DateTime date) {
		string year = date.Year.ToString ();
		string month = date.Month.ToString ();
        if (month.Length < 2) month = "0" + month;
		string day = date.Day.ToString ();
        if (day.Length < 2) day = "0" + day;

		string hour = date.Hour.ToString ();
        if (hour.Length < 2) hour = "0" + hour;
		string min = date.Minute.ToString ();
        if (min.Length < 2) min = "0" + min;
		string sec = date.Second.ToString ();
        if (sec.Length < 2) sec = "0" + sec;

		return string.Format ("{0}/{1}/{2} {3}:{4}:{5}", day, month, year, hour, min, sec);
	}

	public static System.DateTime ConvertSqlToDate (string sql) {
		string date = sql;

		int day = 0;
		int month = 0;
		int year = 0;

		int hour = 0;
		int min = 0;
		int sec = 0;
		//int nano = 0;

		if (sql.Contains (" ")) {
			date = sql.Split (' ') [0];
			string time = sql.Split (' ') [1];
			if (time.Contains ("."))
				time = time.Substring (0, time.IndexOf ('.'));

			day = int.Parse (date.Split ('/')[0]);
			month = int.Parse (date.Split ('/')[1]);
			year = int.Parse (date.Split ('/')[2]);

			hour = int.Parse (time.Split (':')[0]);
			min = int.Parse (time.Split (':')[1]);
			sec = int.Parse (time.Split (':')[2]);

			return new DateTime (year, month, day, hour, min, sec);
		}
		day = int.Parse (sql.Split ('/')[0]);
		month = int.Parse (sql.Split ('/')[1]);
		year = int.Parse (sql.Split ('/')[2]);

		return new DateTime (year, month, day);
	}

    /// <summary>
    /// Como parsear o retorno: age[0] + " anos" + (int.Parse(age[1]) > 0 ? " e " + age[1] + " meses" : "");
    /// </summary>
    /// <param name="dateOfBirth">Dia do nascimento</param>
    /// <returns>Retorna ANOS.MESES</returns>
    public static string AgeInYearsMonths(DateTime? dateOfBirth)
    {
        if (dateOfBirth == null) return "";
        if (dateOfBirth >= DateTime.Today)
        {
            Debug.LogWarning("DateOfBirth cannot be in future!");
            return "0.0";
        }

        DateTime d = dateOfBirth.Value;
        int monthCount = 0;
        while ((d = d.AddMonths(1)) <= DateTime.Today)
        {
            monthCount++;
        }
        return string.Format("{0}.{1}", monthCount / 12, monthCount % 12);
    }

	
	public static int AgeInYears(DateTime dateOfBirth){
		DateTime oneYearTime = new DateTime(1, 1, 1);
		TimeSpan diference = DateTime.Now-dateOfBirth;
		DateTime dateYearsPlusOne = (oneYearTime+diference);
		// because we start at year 1 for the Gregorian calendar, we must subtract a year here.
		int ret = dateYearsPlusOne.Year-1;
		return ret;
	}
	
	/// <summary>
	/// Pega o nome do mês.
	/// </summary>
	/// <param name="month">Mês com Janeiro = 1.</param>
	public static string GetMonthPt(int month){
		return GetMonthPtFirstUpper(month).ToLowerInvariant();
	}

	/// <summary>
	/// Pega o nome do mês com primeira letra maiúscula.
	/// </summary>
	/// <param name="month">Mês com Janeiro = 1.</param>
	public static string GetMonthPtFirstUpper(int month){
		return new string[]{"Não definido","Janeiro", "Fevereiro", "Março", "Abril","Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro","Novembro", "Dezembro"}[month];
	}
}
