﻿//Copyright® do TuTu™

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using TagFilterInternal;
using UnityEngine;

/// <summary>
/// Buscador de objetos através de tags Search("Metal Espada") e retorna as espadas de metal.
/// </summary>
public class TagFilter<T>
{   
    struct IndexedMatches
    {
		public readonly string Tag;
		public readonly List<T> Content;

        public IndexedMatches(string tag, List<T> content)
        {
			Tag = tag;
			Content = content;
        }            
    }

    readonly Dictionary<string, List<T>> indexedData;

    List<IndexedMatches> processedtags;
    Func<T, string> selector;     
    Action<T[]> resultsCallback;

    Thread activeSearchThread;

    TagFilterHelper tagFilterHelper;

    /// <param name="data">Dados para retornar quando ele for compativel com a tag pesquisada..</param>
    /// <param name="selector">Colocar as TAGS. Como se fosse o Select do Linq. Por exemplo: x => x.MyTags.</para
    /// <param name = "resultsCallback">Callback que recebe os dados compativeis com a tag</param>m>
    /// <example> Caso as tags estejam em multiplas propriedades: x => x.Name + " " + x.MyTags</example>
    public TagFilter(T[] data, Func<T, string> selector, Action<T[]> resultsCallback = null)
    {                       
        indexedData = new Dictionary<string, List<T>>();
        processedtags = new List<IndexedMatches>();
        this.selector = selector;
        this.resultsCallback = resultsCallback;

        if(resultsCallback != null)
        {
            //Helper para chamadas assíncronas.
            tagFilterHelper = new GameObject().AddComponent<TagFilterHelper>();
            tagFilterHelper.hideFlags = HideFlags.HideInHierarchy;
            //object não consegue armazenar array, mas consegue armazenar lista. Então faz um unbox de object pra List<T> e depois converte pra Array.
            tagFilterHelper.callback = x => resultsCallback(((List<T>)x).ToArray());          
        }

        IndexNewData(data);
    }

    //"OnDestroy"
    ~TagFilter()
    {
        tagFilterHelper.SelfDestroy();
    }

    /// <summary>c
    /// Insere novos conteúdos no dicionário.
    /// </summary>
    /// <param name="newData">New data.</param>
    public void AddData(params T[] newData)
    {
        IndexNewData(newData);
    }

    /// <summary>
    /// Busca pelas tags e retorna os resultados sem ativar o callback. 
    /// </summary>
    /// <param name="input">Input.</param>
    public T[] Filter(string input)
    {
        return ProcessSearch(input, false);
    }

    /// <summary>
    /// Busca pelas tags em outra Thread e retorna os resultados no callback. 
    /// </summary>
    /// <param name="input">Input.</param>
    public void FilterAsync(string input)
    {
        if (resultsCallback == null)
        {
            Debug.LogError("To make assyncronous calls, you need to define a callback on the class constructor.");
            return;
        }

        tagFilterHelper.data = null;
        KillThread(activeSearchThread);
        activeSearchThread = new Thread(x => ProcessSearch((string)x, true));
        activeSearchThread.Start(input);       
    }

    T[] ProcessSearch(string input, bool asyncCall)
    {
        if (string.IsNullOrEmpty(input))
        {
            processedtags.Clear();
            if (asyncCall)
                tagFilterHelper.data = new List<T>();
            return new T[0];
        }

        //Separa em palavras chaves para processar
        List<string> tagsToProcess = input.ToUpper().Split(new []{ ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
               
        /*
            O indice em que o input diverge do cache. 
            input = A B D F G
            cache = A B C D E
            divergenceAtIndex = 2
        */
        int divergenceAtIndex = -1;

        int tagToProcessCount = tagsToProcess.Count;

        /*
            TODO Mudar a forma linear em que as tags são tradas e avaliar cada uma individualmente. 
            Pois Se você tem um texto grande e muda só a primeira tag, ele vai recalcular tudo
            por causa do índice de divergÊncia.
        */
        for (int i = 0; i < processedtags.Count; i++)
        {            
            if (i >= tagsToProcess.Count)
            {
                divergenceAtIndex = i;
                break;
            }

            //Se o indice cacheado for diferente do indice digitado, temos uma divergência.
            if (processedtags[i].Tag != tagsToProcess[i])
            {                
                divergenceAtIndex = i;
                break;
            }
        }       

        //Remove as tags divergentes do cache
        if (divergenceAtIndex > -1)
        {
            int indexesToPreserve = 0;
            for (int i = 0; i < divergenceAtIndex; i++)
            {
                indexesToPreserve += processedtags[i].Content.Count;
            }

			//Remove as tags antes da divergência. Para não serem processadas novamente, já que elas já estão no cache.
			tagsToProcess.RemoveRange(0, divergenceAtIndex);
            //Remove todos os caches pra frente da divergência (talvez dê pra otimizar melhor?)
            processedtags.RemoveRange(divergenceAtIndex, processedtags.Count - divergenceAtIndex);
        }

        int tagsCount = tagsToProcess.Count;

        foreach (KeyValuePair<string, List<T>> entry in indexedData)
        {
            //Pergunta se a chave indexada começa com alguma das tags inseridas.
            for (int i = 0; i < tagsCount; i++)
            {
                string tag = tagsToProcess[i];

                if (entry.Key.StartsWith(tag))
                {
                    int matchIndex = processedtags.FindIndex(x => x.Tag == tag);

                    if (matchIndex == -1)
                    {
                        //Se a key não estiver no cache, adiciona
                        processedtags.Add(new IndexedMatches(tag, entry.Value));
                    }
                    else
                    {
                        //Se já tiver a key para esses objetos no cache, insere mais elementos que começam com a tag inserida.
                        processedtags[matchIndex] = new IndexedMatches(tag, processedtags[matchIndex].Content.Union(entry.Value).ToList());
                    }                  
                }
            }
        }

        //Significa que não foi possivel obter um match para a ultima palavra, logo não temos items que possuam todas essas tags.
        if (processedtags.Count < tagToProcessCount)
        {
			if (asyncCall)
			{
				tagFilterHelper.data = new List<T>();
				return null;
			}
            return new T[0];
        }

        List<T> finalResults = new List<T>();

        if (processedtags.Count > 0)
        {
            finalResults = processedtags[0].Content;
        }

        for (int i = 1, iMax = processedtags.Count; i < iMax; i++)
        {
            //Adiciona tudo numa lista evitando duplicatas.
            finalResults = finalResults.Intersect(processedtags[i].Content).ToList();            
        }


        if (asyncCall)
        {
			//Se for chamada assíncrona, envia os dados para o helper enviar para main thread.
            tagFilterHelper.data = finalResults;
            return null;
        }
        else
        {
            return finalResults.ToArray();
        }
    }

    /// <summary>
    /// Insere novos dados no dicioanrio
    /// </summary>
    /// <param name="newData">New data.</param>
    void IndexNewData(T[] newData)
    {
		if(newData == null)
		{
			return;
		}

        for (int i = 0; i < newData.Length; i++)
        {
            T data = newData[i];

            //Obtém as tags a partir do seletor definido pelo usuário, e as separa pelos espaços.
            string[] tags = selector(newData[i]).Split(new[]{' '}, StringSplitOptions.RemoveEmptyEntries);

            for (int j = 0; j < tags.Length; j++)
            {
                string tag = tags[j];

                if(indexedData.ContainsKey(tag))
                {
                    //Se o dicionário já possui a Key, adiciona mais conteúdo relacionado a ela.
                    indexedData[tag].Add(data);
                }
                else
                {
                    //Cria uma nova key para o dicionario.
                    List<T> dataList = new List<T>();
                    dataList.Add(data);
                    indexedData.Add(tag, dataList);
                }
            }
        }
    }

    /// <summary>
    /// Faz um Log mostrando os dados indexados.
    /// </summary>
    public void LogIndexedDatabase()
    {
        StringBuilder sb = new StringBuilder();
        List<KeyValuePair<string, List<T>>> sortedDatabase = indexedData.OrderBy(x => x.Key).ToList();

        foreach (KeyValuePair<string, List<T>> entry in sortedDatabase)
        {
            sb.AppendLine().Append("Cached Key: ").Append(entry.Key).Append(" Values: ");
            for (int i = 0; i < entry.Value.Count; i++)
            {
                sb.Append(entry.Value[i]).Append(", ");
            }
        }
        Debug.Log(sb.ToString());
    }

    /*
        Não é recomendado dar kill na thread Imediatamente, por que ela acaba sendo destruída
        antes de liberar os locks. Mas como nesse caso o lock não está sendo usado, não há risco
        de ficar preso.
    */
	#pragma warning disable 0618
    [SecurityPermissionAttribute(SecurityAction.Demand, ControlThread = true)]
    void KillThread(Thread thread)
    {
        if(thread != null)
            thread.Abort();
    }
}