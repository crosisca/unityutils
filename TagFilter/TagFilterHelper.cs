﻿using UnityEngine;
using System;

namespace TagFilterInternal
{    
	/// <summary>
	/// Helper que transfere os resultados da busca para a main thread da unity.
	/// </summary>
	public class TagFilterHelper : MonoBehaviour
    {
        public Action<object> callback;
        public object data;

        bool selfDestroy;

        void Update()
        {
            if(data != null)
            {
                callback(data);
                data = null;
            }

            if(selfDestroy)
            {
                DestroyImmediate(gameObject);
            }                
        }

        public void SelfDestroy()
        {
            selfDestroy = true;
        }
    }
}