using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FarofaStudios.UtilsInternal;
using FarofaStudios.Utils;
using System.Linq;


public static class SoundManager 
{		
	private static bool mMuted;
	
	public static bool Muted
	{
		get{return mMuted;}
	}

	private static GlobalSoundsLibrary globalSounds;
	public static Dictionary<string,AudioClip> SoundsDictionary
	{
		get
		{
			if(!Initialized)
			{
				Initialize();	
			}
			return helper.lastDictionaryBuilt;
		}
	}
	private static bool Initialized = false;
	private static Transform soundsContainer;
	private static SoundManagerHelperObject helper;

	private static List<AudioSource> activeBGM = new List<AudioSource>();
	private static List<AudioSource> activeSFX = new List<AudioSource>();

	private static void Initialize()
	{			
		if(!Initialized)
		{
			soundsContainer = new GameObject("Sounds Container").transform;	
			Object.DontDestroyOnLoad(soundsContainer);
			helper = new GameObject("Sound Manager Helper Object", typeof(SoundManagerHelperObject)).GetComponent<SoundManagerHelperObject>();
			helper.gameObject.hideFlags = HideFlags.HideInHierarchy;
		}
		//Junta todas as listas no mesmo dicionario no awake
		globalSounds = GlobalSoundsLibrary.GlobalLibrary;
		helper.lastDictionaryBuilt = new Dictionary<string, AudioClip>();

		for(int i=0; i < GlobalSoundsLibrary.GlobalLibrary.audioNames.Count; i++)
		{
			if(helper.lastDictionaryBuilt.ContainsKey(globalSounds.audioNames[i]))
				throw new UnityException("Alguma biblioteca ja possui um som com o mesmo nome: " + globalSounds.audioNames[i]);
			else
				helper.lastDictionaryBuilt.Add(globalSounds.audioNames[i], globalSounds.audioClips[i]);			
		}
		
		SoundsLibrary[] localSounds = Object.FindObjectsOfType<SoundsLibrary>();

		for(int i=0; i < localSounds.Length; i++)
		{
			for (int j=0; j < localSounds[i].AudioNames.Count; j++)
			{
				if(helper.lastDictionaryBuilt.ContainsKey (localSounds [i].audioNames [j]))
					Debug.LogError ("Alguma biblioteca ja possui um som com o mesmo nome: " + localSounds[i].audioNames[j], localSounds [i]);
				else
					helper.lastDictionaryBuilt.Add(localSounds[i].AudioNames[j], localSounds[i].AudioClips[j]);
			}
		}
		Initialized = true;
	}

	private static AudioSource PlaySound(string soundName, bool loop)
	{
		return PlaySound(soundName,1,0,loop);
	}

	private static AudioSource PlaySound(string soundName, float volume=1, float delay=0, bool loop=false)
	{
		if(string.IsNullOrEmpty (soundName))
			return null;
		
		RemoveStoppedAudios();
		if(!mMuted)
		{
			AudioClip audioClip;
	
			if(SoundsDictionary.TryGetValue(soundName, out audioClip))
			{
				GameObject emitter = new GameObject(soundName + "emitter");
				emitter.transform.parent = soundsContainer;
				AudioSource emitterSource = emitter.AddComponent<AudioSource>();
				emitterSource.loop = loop;
				emitterSource.volume = volume;
				emitterSource.clip = audioClip;
				
				emitterSource.PlayDelayed(delay);

				if(!loop)
					Object.Destroy(emitter, emitterSource.clip.length + delay + 1f);

				return emitterSource;				
			}
			else
			{
				Debug.LogWarning("Som não encontrado. Som: " + soundName);
			}
		}
		return null;		
	}

	public static AudioSource PlaySFX(string soundName, bool loop)
	{
		return PlaySFX(soundName,1,0,loop);
	}
	
	public static AudioSource PlaySFX(string soundName, float volume=1, float delay=0, bool loop=false)
	{
		AudioSource audioSource = PlaySound(soundName, volume, delay, loop);
		activeSFX .Add(audioSource);
		return audioSource;
	}

	/// Os sons tocados através desse metodo não serão inclusos no comando StopAllSFX();
	public static AudioSource PlayUnlistedSFX(string soundName, bool loop)
	{
		return PlayUnlistedSFX(soundName,1,0,loop);
	}

	/// Os sons tocados através desse metodo não serão inclusos no comando StopAllSFX();
	public static AudioSource PlayUnlistedSFX(string soundName, float volume=1, float delay=0, bool loop=false)
	{
		return PlaySound(soundName, volume, delay, loop);;
	}
	
	public static void StopAllSFXs(){
		foreach(AudioSource se in activeSFX ){
			if(se!=null)
				Object.Destroy(se.gameObject);
		}
		RemoveStoppedAudios();
	}
	
	public static AudioSource PlayBGM(string soundName, float volume=1, float delay=0)
	{
		AudioSource audioSource = PlaySound(soundName, volume, delay, true);
		activeBGM.Add(audioSource);
		return audioSource;
	}

	/// Os sons tocados através desse metodo não serão inclusos no comando StopAllBGM();
	public static AudioSource PlayUnlistedBGM(string soundName, float volume=1, float delay=0)
	{
		return PlaySound(soundName, volume, delay, true);
	}

	public static void StopAllBGMs(){
		RemoveStoppedAudios();
		foreach(AudioSource bgm in activeBGM){
			if(bgm!=null)
				Object.Destroy(bgm.gameObject);
		}
	}
	
	public static AudioSource CrossfadeSoundEffect(AudioSource fadeOutTrack, string soundName)
	{
		return CrossfadeSoundEffect(fadeOutTrack, soundName, 1);
	}
	
	public static AudioSource CrossfadeSoundEffect(AudioSource fadeOutTrack, string soundName, float volume)
	{
		AudioSource newSound = PlaySFX(soundName, volume);
		
		if(fadeOutTrack != null)			
			helper.StartCoroutine(FadeSoundRoutine(fadeOutTrack, false, true));
		
		helper.StartCoroutine(FadeSoundRoutine(newSound, true, false));				
		
		return null;
	}
	
	public static void FadeSound(AudioSource source, bool fadeIn, bool destroyOnEnd = false)
	{
		helper.StartCoroutine(FadeSoundRoutine(source, fadeIn, destroyOnEnd));
	}
	
	private static IEnumerator FadeSoundRoutine(AudioSource source, bool fadeIn, bool destroyOnEnd)
	{
		while(source != null && (fadeIn ? source.volume < 1 : source.volume > 0))
		{
			source.volume += (fadeIn) ? 0.05f : -0.05f;
			yield return null;
		}

		if(destroyOnEnd)
			Object.Destroy(source);
	}
	
	public static void Mute(bool state)
	{
		mMuted = state;
	}
	
	public static void RebuildDictionary()
	{
		Initialize();
	}
	/*public static void ChangeLocalLibrary(LocalSounds localLibrary)
	{
		mLocalSounds = localLibrary;
	}*/

	/// <summary>
	/// Remove os audios que pararam de tocar ou foram destruídos do Array.
	/// </summary>
	public static void RemoveStoppedAudios(){
		for(int seCount=0;seCount<activeSFX .Count;seCount++){
			if(activeSFX [seCount]==null || !activeSFX [seCount].isPlaying){
				activeSFX .RemoveAt(seCount);
				seCount--;
			}
		}
		for(int bgmCount=0;bgmCount<activeBGM.Count;bgmCount++){
			if(activeBGM[bgmCount]==null || !activeBGM[bgmCount].isPlaying){
				activeBGM.RemoveAt(bgmCount);
				bgmCount--;
			}
		}
	}

	public static void Dispose()
	{
		SoundsDictionary.Clear ();
		SoundsLibrary[] localSounds = Object.FindObjectsOfType<SoundsLibrary>().Where (x => x.name == "WordSounds").ToArray ();

		foreach(SoundsLibrary library in localSounds)
		{
			foreach (AudioClip clip in library.audioClips)
			{					
				clip.UnloadAudioData ();
			}

		}		
	}

	public static void ManuallyLoadSounds(string[] sounds)
	{
		foreach(string sound in sounds)
		{
			AudioClip clip;
			SoundsDictionary.TryGetValue (sound, out clip);
			clip.LoadAudioData ();
		}
	}
}

