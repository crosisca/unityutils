﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;
using FarofaStudios.Utils;

namespace FarofaStudios.UtilsInternal
{
	[CustomEditor(typeof(SoundsLibrary))]
	public class SoundsLibraryEditor : Editor 
	{
		public static readonly string assetPath = "Assets/FarofaSoundManager/Resources/GlobalSoundsLibrary.asset";
		private SoundsLibrary soundsLibrary;	
		private GlobalSoundsLibrary globalSoundsLibrary;
		private LibraryAsset library;
		
		private int DictionarySize
		{
			get
			{
				return soundsLibrary.DictionarySize;
			}
			
			set
			{
				soundsLibrary.DictionarySize = value;
			}
		}	
		
		private int GlobalDictionarySize
		{
			get
			{			
				return globalSoundsLibrary.DictionarySize;
			}
			
			set
			{
				globalSoundsLibrary.DictionarySize = value;
			}
		}	
		
		private bool unfoldGlobalSounds = false;
		private bool unfoldLocalSounds = true;
		private bool hasPro;
		Vector2 ScrollViewPosition;

		private void Awake()
		{
			soundsLibrary = (SoundsLibrary)target;
			hasPro = UnityEditorInternal.InternalEditorUtility.HasPro();
			
			if(library == null)
			{
				library = AssetDatabase.LoadAssetAtPath(SoundsLibraryEditor.assetPath, typeof(LibraryAsset)) as LibraryAsset;
				
				if(library == null)
				{
					library = ScriptableObject.CreateInstance<LibraryAsset>();
					AssetDatabase.CreateAsset(library, SoundsLibraryEditor.assetPath);
				}
				
				if(library.GlobalSoundsLibrary == null)
				{
					library.GlobalSoundsLibrary = new GlobalSoundsLibrary();
				}			
			}	
			globalSoundsLibrary = library.GlobalSoundsLibrary;			
		}
			
		public override	void OnInspectorGUI()
		{
			DrawDefaultInspector();			
			unfoldGlobalSounds = EditorGUILayout.Foldout(unfoldGlobalSounds, "Global Dictionary");
		
			if(unfoldGlobalSounds)
			{				
				EditorGUILayout.BeginHorizontal();
				GUI.backgroundColor = (hasPro) ? new Color(0.4f,0.4f,0.4f): new Color(0.9f,0.9f,0.9f);
				
				EditorGUILayout.LabelField("Global Size", GUILayout.MaxWidth(80f));		
				GlobalDictionarySize = EditorGUILayout.IntField(GlobalDictionarySize, GUILayout.MinWidth(10f), GUILayout.MaxWidth(50f));		
				
				if(GUILayout.Button("+", GUILayout.MinWidth(10f), GUILayout.MaxWidth(20f)))
				{
					GlobalDictionarySize += 1;
				}
				if(GUILayout.Button("-", GUILayout.MinWidth(10f), GUILayout.MaxWidth(20f)))
				{
					GlobalDictionarySize -= 1;
				}
				EditorGUILayout.EndHorizontal();
				
				if(GlobalDictionarySize > 0)
				{
					EditorGUILayout.BeginHorizontal();		
					EditorGUILayout.LabelField("Global Audio Name");
					EditorGUILayout.LabelField("Global Audio Clip");		
					EditorGUILayout.EndHorizontal();							
					
					for(int i=0; i < GlobalDictionarySize; i++)
					{
						ShowContent(i, globalSoundsLibrary.audioNames, globalSoundsLibrary.audioClips, true);				
					}
					
					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.Space();
					GUI.backgroundColor = new Color(0.7f,0.7f,0.7f);
					if(GUILayout.Button("New Global Sound"))
					{
						GlobalDictionarySize += 1;
					}
					EditorGUILayout.EndHorizontal();		
					
				}
			}
			
			unfoldLocalSounds = EditorGUILayout.Foldout(unfoldLocalSounds, "Local Dictionary");	
					
			if(unfoldLocalSounds)
			{	
				GUI.backgroundColor = new Color(1f,1f,1f);				
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Size", GUILayout.MaxWidth(40f));		
				DictionarySize = EditorGUILayout.IntField(DictionarySize, GUILayout.MinWidth(10f), GUILayout.MaxWidth(50f));		
				
				GUI.backgroundColor = (hasPro) ? new Color(0.4f,0.4f,0.4f): new Color(0.9f,0.9f,0.9f);
				if(GUILayout.Button("+", GUILayout.MinWidth(10f), GUILayout.MaxWidth(20f)))
				{
					DictionarySize += 1;
				}
				if(GUILayout.Button("-", GUILayout.MinWidth(10f), GUILayout.MaxWidth(20f)))
				{
					DictionarySize -= 1;
				}
				EditorGUILayout.EndHorizontal();
				
				if(DictionarySize > 0)
				{
					EditorGUILayout.BeginHorizontal();		
					EditorGUILayout.LabelField("Audio Name");
					EditorGUILayout.LabelField("Audio Clip");		
					EditorGUILayout.EndHorizontal();

					if(DictionarySize > 25)
						ScrollViewPosition = EditorGUILayout.BeginScrollView (ScrollViewPosition, false, true);
					
				
					for(int i=0; i < DictionarySize; i++)
					{	
						ShowContent(i, soundsLibrary.AudioNames, soundsLibrary.AudioClips, false);				
					}

					if(DictionarySize > 25)
						EditorGUILayout.EndScrollView();	
					
					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.Space();
					GUI.backgroundColor = new Color(0.7f,0.7f,0.7f);
					if(GUILayout.Button("New Sound"))
					{
						DictionarySize += 1;
					}
					EditorGUILayout.EndHorizontal();				
				}
			}
			else
			{
				ScrollViewPosition = Vector2.zero;
			}

			object[] draggedObjects = DropAreaGUI ();
			if(draggedObjects != null)
			{
				if(soundsLibrary.AudioClips == null)
				{
					soundsLibrary.AudioClips = new List<AudioClip> ();
					soundsLibrary.AudioNames = new List<string> ();
				}

				foreach(object obj in draggedObjects)
				{
					AudioClip clip = (AudioClip)obj;
					soundsLibrary.AudioClips.Add (clip);
					soundsLibrary.AudioNames.Add (clip.name);
				}
			}
		}

		private void ShowContent(int index, List<string> audioNames, List<AudioClip> audioClips, bool isGlobal)
		{
			GUI.backgroundColor = (index%2 == 0) ? (hasPro) ? new Color(0.5f,0.5f,0.5f) : new Color(0.9f,0.9f,0.9f) : new Color(0.7f,0.7f,0.7f); 
	
			EditorGUILayout.BeginHorizontal();		
			
			audioNames[index] = EditorGUILayout.TextField(audioNames[index], GUILayout.MinWidth(5f));
			audioClips[index] = EditorGUILayout.ObjectField(audioClips[index], typeof(AudioClip), false, GUILayout.MinWidth(5f)) as AudioClip;
			
			if(GUILayout.Button("Play", GUILayout.MinWidth(10f), GUILayout.MaxWidth(40f)))
			{
				if(isGlobal)
				{
					if(globalSoundsLibrary.audioClips[index] != null)
					{
						PlayClip(globalSoundsLibrary.audioClips[index]);
					}
				}
				else
				{
					if(soundsLibrary.AudioClips[index] != null)
					{					
						PlayClip(soundsLibrary.AudioClips[index]);
					}
				}
				
			}
			GUILayout.Space(-5);
			if(GUILayout.Button("-", GUILayout.MinWidth(10f), GUILayout.MaxWidth(20f)))
			{
				DictionaryRemoveAt(index, audioNames, audioClips);
			}
			EditorGUILayout.EndHorizontal();	
					
		}
		
		private void DictionaryRemoveAt(int index, List<string> audioNames, List<AudioClip> audioClips)
		{			
			audioNames.RemoveAt(index);
			audioClips.RemoveAt(index);
		}
	
		object[] DropAreaGUI ()
		{
			Event evt = Event.current;
			Rect drop_area = GUILayoutUtility.GetRect (0.0f, 25.0f, GUILayout.ExpandWidth (true));
			GUI.Box (drop_area, "Drag Multiple LocalSounds Here");

			switch (evt.type) {
				case EventType.DragUpdated:
				case EventType.DragPerform:
					if(!drop_area.Contains (evt.mousePosition))
						return null;

					DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

					if (evt.type == EventType.DragPerform) {
						DragAndDrop.AcceptDrag ();

						return DragAndDrop.objectReferences;
					}
					break;
			}

			return null;
		}

		private void PlayClip(AudioClip clip)
		{
			Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
			System.Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
			MethodInfo method = audioUtilClass.GetMethod(
				"PlayClip",
				BindingFlags.Static | BindingFlags.Public,
				null,
				new System.Type[] {
				typeof(AudioClip)
			},
			null
			);
			method.Invoke(
				null,
				new object[] {
				clip
			}
			);
			
		}
		
		private void StopClip(AudioClip clip)
		 {
			
			Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
			System.Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
			MethodInfo method = audioUtilClass.GetMethod(
				"StopClip",
				BindingFlags.Static | BindingFlags.Public,
				null,
				new System.Type[] {
				typeof(AudioClip)
			},
			null
			);
			method.Invoke(
				null,
				new object[] {
				clip
			}
			);
		}
		
		private bool IsClipPlaying(AudioClip clip)
		{
			Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
			System.Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
			MethodInfo method = audioUtilClass.GetMethod(
				"IsClipPlaying",
				BindingFlags.Static | BindingFlags.Public,
				null,
				new System.Type[] {
				typeof(AudioClip)
			},
			null
			);
			return (bool)method.Invoke(
				null,
				new object[] {
				clip
			}
			);
		}
		
		private float GetClipPosition(AudioClip clip)
		{
			Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
			System.Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
			MethodInfo method = audioUtilClass.GetMethod(
				"GetClipPosition",
				BindingFlags.Static | BindingFlags.Public,
				null,
				new System.Type[] {
				typeof(AudioClip)
			},
			null
			);
			return (float)method.Invoke(
				null,
				new object[] {
				clip
			}
			);
		}
	}
}