using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Collections.Generic;

namespace FarofaStudios.UtilsInternal
{
	public class GlobalSoundLibraryEditorWindow : EditorWindow 
	{	
		private Vector2 scrollPos;
		private static GlobalSoundsLibrary globalSoundLibrary;
		[SerializeField]
		public List<string> audioNamesList;
		[SerializeField]
		public List<AudioClip> audioClipList;
		private static LibraryAsset libraryAsset;
		private static bool hasPro;
		private int DictionarySize
		{
			get
			{
				return globalSoundLibrary.DictionarySize;
			}
			
			set
			{
				globalSoundLibrary.DictionarySize = value;
			}
		}		
		
		[MenuItem ("FarofaStudios/Sound Library/Global Sounds",false,6)]
		static void Init () 
		{
			hasPro = UnityEditorInternal.InternalEditorUtility.HasPro();
			GlobalSoundLibraryEditorWindow window = (GlobalSoundLibraryEditorWindow)EditorWindow.GetWindow (typeof (GlobalSoundLibraryEditorWindow));
			window.titleContent = new GUIContent("Global Sound Library");	
			if(libraryAsset == null)
			{
				libraryAsset = AssetDatabase.LoadAssetAtPath(SoundsLibraryEditor.assetPath, typeof(LibraryAsset)) as LibraryAsset;
				
				if(libraryAsset == null)
				{
					libraryAsset = ScriptableObject.CreateInstance<LibraryAsset>();
					AssetDatabase.CreateAsset(libraryAsset, SoundsLibraryEditor.assetPath);
				}
				
				if(libraryAsset.GlobalSoundsLibrary == null)
				{
					libraryAsset.GlobalSoundsLibrary = new GlobalSoundsLibrary();
				}			
			}	
			globalSoundLibrary = libraryAsset.GlobalSoundsLibrary;
			
			
			
			
			
			window.Show();	
		}
				
		public void OnGUI () 
		{
			if(globalSoundLibrary == null)
				Init ();
				
			GUILayout.Label ("Global Sound Library", EditorStyles.boldLabel);
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Size", GUILayout.MaxWidth(40f));		
			DictionarySize = EditorGUILayout.IntField(DictionarySize, GUILayout.MinWidth(10f), GUILayout.MaxWidth(50f));		
			
			GUI.backgroundColor = (hasPro) ? new Color(0.4f,0.4f,0.4f): new Color(0.9f,0.9f,0.9f);;
			if(GUILayout.Button("+", GUILayout.MinWidth(10f), GUILayout.MaxWidth(20f)))
			{
				DictionarySize += 1;
			}
			if(GUILayout.Button("-", GUILayout.MinWidth(10f), GUILayout.MaxWidth(20f)))
			{
				DictionarySize -= 1;
			}
			EditorGUILayout.EndHorizontal();
			
			if(DictionarySize > 0)
			{
				EditorGUILayout.BeginHorizontal();		
				EditorGUILayout.LabelField("Audio Name");
				EditorGUILayout.LabelField("Audio Clip");		
				EditorGUILayout.EndHorizontal();		
				scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
				for(int i=0; i < DictionarySize; i++)
				{	
					GUI.backgroundColor = (i%2 == 0) ? (hasPro) ? new Color(0.4f,0.4f,0.4f) : new Color(0.9f,0.9f,0.9f) : new Color(0.6f,0.6f,0.6f); 
					EditorGUILayout.BeginHorizontal();		
					GUILayout.Space(10f);
					
					globalSoundLibrary.audioNames[i] = EditorGUILayout.TextField(globalSoundLibrary.audioNames[i], GUILayout.MinWidth(5f));
					globalSoundLibrary.audioClips[i] = EditorGUILayout.ObjectField(globalSoundLibrary.audioClips[i], typeof(AudioClip), false, GUILayout.MinWidth(5f)) as AudioClip;
					
					if(GUILayout.Button("Play", GUILayout.MinWidth(10f), GUILayout.MaxWidth(40f)))
					{
						if(globalSoundLibrary.audioClips[i] != null)
						{
							PlayClip(globalSoundLibrary.audioClips[i]);
						}
					}
					GUILayout.Space(-5);
					
					if(GUILayout.Button("-", GUILayout.MinWidth(10f), GUILayout.MaxWidth(20f)))
					{
						DictionaryRemoveAt(i);
					}
					EditorGUILayout.EndHorizontal();					
				}			
				EditorGUILayout.EndScrollView();			
				
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.Space();
				GUI.backgroundColor = new Color(0.4f,0.4f,0.4f);
				if(GUILayout.Button("New Global Sound"))
				{
					DictionarySize += 1;
				}
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.Space();
				EditorGUILayout.Space();
				EditorGUILayout.Space();			
			}
		}
		
		private void DictionaryRemoveAt(int index)
		{
			globalSoundLibrary.audioNames.RemoveAt(index);
			globalSoundLibrary.audioClips.RemoveAt(index);		
		}	
		
		private static void PlayClip(AudioClip clip)
		{
			Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
			System.Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
			MethodInfo method = audioUtilClass.GetMethod(
				"PlayClip",
				BindingFlags.Static | BindingFlags.Public,
				null,
				new System.Type[] {
				typeof(AudioClip)
			},
			null
			);
			method.Invoke(
				null,
				new object[] {
				clip
			}
			);
		}
	}	
}
	
	