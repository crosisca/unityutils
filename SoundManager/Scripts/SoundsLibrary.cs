﻿using UnityEngine;
using System;
using System.Collections.Generic;


namespace FarofaStudios.Utils
{
	[Serializable]
	public class SoundsLibrary : MonoBehaviour 
	{
		[HideInInspector]public List<string> audioNames;
		[HideInInspector]public List<AudioClip> audioClips;	
		
		public List<string> AudioNames
		{
			get
			{
				return audioNames;
			}
			set
			{
				audioNames = value;
			}
		}
		
		public List<AudioClip> AudioClips
		{
			get
			{
				return audioClips;
			}
			set
			{
				audioClips = value;
			}
		}
		
		public int DictionarySize
		{
			get
			{			
				if(audioNames != null)
				{
					return audioNames.Count;
				}
				else
				{								
					return 0;
				}
			}
			
			set
			{
				if(value < 1)
				{
					audioNames = null;
					audioClips= null;
				} 
				else if(audioClips == null)
				{
					audioNames = new List<string>();
					audioClips = new List<AudioClip>();	
					audioNames.Add("");
					audioClips.Add(null);
				}
				else
				{
					if(value > audioNames.Count)
					{
						int difference = value - DictionarySize;
						for(int i=0; i < difference; i++)
						{
							audioNames.Add("");
							audioClips.Add(null);
						}
					}
					else if(value < audioNames.Count)
					{
						int difference = audioNames.Count - value;					
						audioNames.RemoveRange(audioNames.Count - difference, difference);
						audioClips.RemoveRange(audioClips.Count - difference, difference);							
					}
				}	
			}
		}
	}
}
