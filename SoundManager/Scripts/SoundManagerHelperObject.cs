﻿using UnityEngine;
using System.Collections.Generic;
using FarofaStudios.Utils;

namespace FarofaStudios.UtilsInternal
{
	[AddComponentMenu("")]
	public class SoundManagerHelperObject : MonoBehaviour
	{
		public Dictionary<string,AudioClip> lastDictionaryBuilt;
		private bool firstTime = true;
		private int currentScene;
		
		private void Awake()
		{
			GameObject.DontDestroyOnLoad(gameObject);
			currentScene = Application.loadedLevel;
		}

		private void OnLevelWasLoaded(int level)
		{
			if(firstTime)
			{
				firstTime = false;
				return;
			}
			SoundManager.RebuildDictionary();
		}
		
		public void Update()
		{
			if(Application.loadedLevel != currentScene)
			{
				SoundManager.RebuildDictionary();
				currentScene = Application.loadedLevel;
			}
		}
	}
}