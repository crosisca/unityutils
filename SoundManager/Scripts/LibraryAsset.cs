﻿using UnityEngine;
using System.Collections.Generic;
using FarofaStudios.Utils;

namespace FarofaStudios.UtilsInternal
{
	public class LibraryAsset : ScriptableObject
	{
		[SerializeField]private GlobalSoundsLibrary globalSoundsLibrary;

		public GlobalSoundsLibrary GlobalSoundsLibrary
		{
			get
			{
				return globalSoundsLibrary;
			}
			set
			{
				globalSoundsLibrary = value;
			}
		}
	}
}