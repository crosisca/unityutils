﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace FarofaStudios.UtilsInternal
{
	[Serializable]
	public class GlobalSoundsLibrary
	{			
		public List<string> audioNames;
		public List<AudioClip> audioClips;	
		
		private static GlobalSoundsLibrary globalSoundsLibrary;
		public static GlobalSoundsLibrary GlobalLibrary
		{
			get
			{
				if(globalSoundsLibrary == null)
				{
					globalSoundsLibrary = Resources.Load<LibraryAsset>("GlobalSoundsLibrary").GlobalSoundsLibrary;
				}
				return globalSoundsLibrary;
			}			
		}
		
		public int DictionarySize
		{
			get
			{			
				if(audioNames != null)
				{
					return audioNames.Count;
				}
				else
				{								
					return 0;
				}
			}
					
			set
			{
				if(value < 1)
				{
					audioNames = null;
					audioClips= null;
				} 
				else if(audioClips == null)
				{
					audioNames = new List<string>();
					audioClips = new List<AudioClip>();	
					audioNames.Add("");
					audioClips.Add(null);
				}
				else
				{
					if(value > audioNames.Count)
					{
						int difference = value - DictionarySize;
						for(int i=0; i < difference; i++)
						{
							audioNames.Add("");
							audioClips.Add(null);
						}
					}
					else if(value < audioNames.Count)
					{
						int difference = audioNames.Count - value;					
						audioNames.RemoveRange(audioNames.Count - difference, difference);
						audioClips.RemoveRange(audioClips.Count - difference, difference);							
					}
				}	
			}
		}	
	}
}
